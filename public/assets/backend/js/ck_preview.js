CKEDITOR.editorConfig = function (config) {
    config.language = 'ru';
    config.height = 150;
    config.allowedContent = true;
    config.disallowedContent = 'br';
    config.format_tags = 'p;h2;h3';
    config.toolbarGroups = [
        {"name": "basicstyles", "groups": ["basicstyles"]},
        {"name": "links", "groups": ["links"]},
        {"name": "paragraph", "groups": ["list", "blocks"]},
        {"name": "document", "groups": ["mode"]},
        {"name": "insert", "groups": ["insert"]},
        {"name": "styles", "groups": ["styles"]}
    ];
    config.removeButtons = 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar';

}