<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_brands', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('country_id')->default(1)->index();
            $table->string('slug')->unique();
            $table->string('title');
            $table->text('preview')->nullable();
            $table->text('content')->nullable();
            $table->string('image')->nullable();
            $table->string('meta_k')->nullable();
            $table->string('meta_d')->nullable();
            $table->timestamps();

            $table->foreign('country_id')
                ->references('id')
                ->on('countries')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shop_brands');
    }
}
