<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicationsArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publications_articles', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('rubric_id');
            $table->string('slug')->unique()->index();
            $table->string('title');
            $table->text('preview')->nullable();
            $table->mediumText('content')->nullable();
            $table->string('image')->nullable();
            $table->boolean('is_enabled')->default(false)->index();
            $table->string('meta_k')->nullable();
            $table->string('meta_d')->nullable();
            $table->timestamps();
            $table->timestamp('published_at')->index();

            $table->foreign('rubric_id')
                ->references('id')
                ->on('publications_rubrics')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('publications_articles', function(Blueprint $table) {
            $table->dropForeign('publications_articles_rubric_id_foreign');
        });

        Schema::drop('publications_articles');
    }
}
