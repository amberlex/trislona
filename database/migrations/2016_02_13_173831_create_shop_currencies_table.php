<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopCurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_currencies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 64);
            $table->char('code', 3)->unique();
            $table->decimal('rate', 8, 4);
            $table->string('symbol', 3);
            $table->enum('symbol_pos', ['left', 'right'])->default('right');
            $table->boolean('is_main')->default(0);
            $table->boolean('is_enabled')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shop_currencies');
    }
}
