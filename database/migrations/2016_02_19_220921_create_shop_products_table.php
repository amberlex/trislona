<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('brand_id')->default(1)->index();
            $table->unsignedInteger('category_id')->index();
            $table->unsignedInteger('currency_id')->index();
            $table->string('slug')->unique();
            $table->string('title');
            $table->text('preview')->nullable();
            $table->mediumText('content')->nullable();
            $table->string('image')->nullable();
            $table->decimal('price', 13,2)->unsigned()->default(0.00);
            $table->decimal('price_old', 13,2)->unsigned()->default(0.00);
            $table->boolean('is_hit')->default(false);
            $table->boolean('is_hot')->default(false);
            $table->boolean('is_action')->default(false);
            $table->boolean('is_enabled')->default(false)->index();
            $table->string('meta_k')->nullable();
            $table->string('meta_d')->nullable();
            $table->timestamps();

            $table->foreign('brand_id')
                ->references('id')
                ->on('shop_brands')
                ->onDelete('restrict');

            $table->foreign('category_id')
                ->references('id')
                ->on('shop_categories')
                ->onDelete('restrict');

            $table->foreign('currency_id')
                ->references('id')
                ->on('shop_currencies')
                ->onDelete('restrict');
        });

        Schema::create('shop_product_category', function (Blueprint $table)
        {
            $table->integer('product_id')->unsigned()->index();
            $table->foreign('product_id')->references('id')->on('shop_products')->onDelete('cascade');

            $table->integer('category_id')->unsigned()->index();
            $table->foreign('category_id')->references('id')->on('shop_categories')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shop_products');
        Schema::drop('shop_product_category');
    }
}
