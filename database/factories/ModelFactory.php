<?php

$factory->define(\App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Publications\Article::class, function ($faker)
{
    $title = $faker->sentence(mt_rand(2, 3));

    return [
        'rubric_id'    => mt_rand(1, 3),
        'slug'         => str_slug($title),
        'title'        => $title,
        'preview'      => $faker->paragraph(mt_rand(3, 6)),
        'content'      => join(PHP_EOL.PHP_EOL, $faker->paragraphs(mt_rand(5, 8))),
        'image'        => $faker->imageUrl($width = 640, $height = 480),
        'is_enabled'   => mt_rand(0, 1),
        'published_at' => $faker->dateTimeBetween('-1 month', '+3 days'),
        'meta_k'       => $title,
        'meta_d'       => $title,
    ];
});

$factory->define(App\Models\Shop\Brand::class, function ($faker)
{
    $title = $faker->unique()->sentence(mt_rand(1, 2));

    return [
        'country_id' => mt_rand(1, 15),
        'slug'       => str_slug($title),
        'title'      => $title,
        'preview'    => $faker->paragraph(mt_rand(3, 6)),
        'content'    => join(PHP_EOL.PHP_EOL, $faker->paragraphs(mt_rand(5, 8))),
        'image'      => 'http://lorempixel.com/640/480/?' . uniqid(),
        'meta_k'     => $title,
        'meta_d'     => $title,
    ];
});
