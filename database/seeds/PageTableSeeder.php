<?php

use Illuminate\Database\Seeder;
use App\Models\Page;

class PageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Page::truncate();

        Page::insert([
            [
                'slug'       => 'dostavka',
                'title'      => 'Доставка',
                'content'    => 'Содержимое страницы Доставка',
                'is_enabled' => 1,
                'icon'       => 'fa-truck',
                'meta_k'     => 'Доставка',
                'meta_d'     => 'Доставка',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'slug'       => 'oplata',
                'title'      => 'Оплата',
                'content'    => 'Содержимое страницы Оплата',
                'is_enabled' => 1,
                'icon'       => 'fa-credit-card',
                'meta_k'     => 'Оплата',
                'meta_d'     => 'Оплата',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'slug'       => 'o-nas',
                'title'      => 'О нас',
                'content'    => 'Содержимое страницы О нас',
                'is_enabled' => 1,
                'icon'       => 'fa-info',
                'meta_k'     => 'О нас',
                'meta_d'     => 'О нас',
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
        ]);
    }
}
