<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // Disable foreign key check for this connection before running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->call(UserTableSeeder::class);
        $this->call(PageTableSeeder::class);
        $this->call(SettingsTableSeeder::class);

        // Publications
        $this->call(PublicationsRubricTableSeeder::class);
        $this->call(PublicationsArticleTableSeeder::class);

        // Shop
        $this->call(ShopCurrencyTableSeeder::class);
        $this->call(ShopBrandsTableSeeder::class);
        $this->call(ShopCategoriesTableSeeder::class);
        $this->call(ShopProductsTableSeeder::class);

        // Enable foreign key check
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        Model::reguard();
    }
}
