<?php

use Illuminate\Database\Seeder;
use App\Models\Shop\Brand;

class ShopBrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Brand::truncate();

        DB::table('shop_brands')->insert(['title' => 'Не указан', 'slug' => 'no-brands']);

        factory(Brand::class, 5)->create();
    }
}
