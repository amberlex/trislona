<?php

use Illuminate\Database\Seeder;
use App\Models\Shop\Category;
use Faker\Factory;

class ShopCategoriesTableSeeder extends Seeder
{
    protected $fake;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::truncate();

        $this->fake = Factory::create();

        $categories = $this->getChildren();
        Category::buildTree($categories);

        foreach (Category::all() as $category) {
            $category->updatePath();
        }
    }

    // TODO optimize!
    private static $id = 1;

    private function getChildren()
    {
        $children[] = [
            'id'          => self::$id++,
            'title'       => 'Без категории',
            'slug'        => 'no-categories',
            'description' => '',
            'image'       => '',
            'meta_k'      => '',
            'meta_d'      => '',
            'created_at'  => date('Y-m-d H:i:s', time()),
            'updated_at'  => date('Y-m-d H:i:s', time()),
        ];

        for ($i = 0; $i < 2; $i++) {
            $title = rtrim($this->fake->unique()->sentence(1), '.');
            $children[] = [
                'id'          => self::$id++,
                'title'       => $title,
                'slug'        => str_slug($title),
                'description' => $this->fake->paragraph(mt_rand(3, 6)),
                'image'       => 'http://lorempixel.com/640/480/?' . uniqid(),
                'meta_k'      => $title,
                'meta_d'      => $title,
                'created_at'  => date('Y-m-d H:i:s', time()),
                'updated_at'  => date('Y-m-d H:i:s', time()),
                'children'    => $this->getChildren2()
            ];
        }

        return $children;
    }

    private function getChildren2()
    {
        $children = [];
        for ($i = 0; $i < 2; $i++) {
            $title = rtrim($this->fake->unique()->sentence(1), '.');
            $children[] = [
                'id'          => self::$id++,
                'title'       => $title,
                'slug'        => str_slug($title),
                'description' => $this->fake->paragraph(mt_rand(3, 6)),
                'image'       => 'http://lorempixel.com/640/480/?' . uniqid(),
                'meta_k'      => $title,
                'meta_d'      => $title,
                'created_at'  => date('Y-m-d H:i:s', time()),
                'updated_at'  => date('Y-m-d H:i:s', time()),
                'children'    => $this->getChildren3()
            ];
        }

        return $children;
    }

    private function getChildren3()
    {
        $children = [];
        for ($i = 0; $i < mt_rand(2, 5); $i++) {
            $title = rtrim($this->fake->unique()->sentence(1), '.');
            $children[] = [
                'id'          => self::$id++,
                'title'       => $title,
                'slug'        => str_slug($title),
                'description' => $this->fake->paragraph(mt_rand(3, 6)),
                'image'       => 'http://lorempixel.com/640/480/?' . uniqid(),
                'meta_k'      => $title,
                'meta_d'      => $title,
                'created_at'  => date('Y-m-d H:i:s', time()),
                'updated_at'  => date('Y-m-d H:i:s', time()),
            ];
        }

        return $children;
    }
}
