<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        User::insert([
            [
                'name'           => 'admin',
                'email'          => 'admin@example.com',
                'password'       => bcrypt('1q2w3e4r'),
                'remember_token' => str_random(10),
                'is_admin'       => true,
            ],
            [
                'name'           => 'user',
                'email'          => 'user@example.com',
                'password'       => bcrypt('1q2w3e4r'),
                'remember_token' => str_random(10),
                'is_admin'       => false,
            ],
        ]);
    }
}
