<?php

use Illuminate\Database\Seeder;
use App\Models\Settings;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Settings::truncate();

        Settings::insert([

            // Global
            [
                'type'  => 'global',
                'field' => 'input',
                'name'  => 'Название сайта',
                'key'   => 'site_name',
                'val'   => 'Три Слона - Магазин товаров для дома',
            ],
            [
                'type'  => 'global',
                'field' => 'input',
                'name'  => 'Краткое название сайта',
                'key'   => 'site_name_short',
                'val'   => 'Три Слона',
            ],
            [
                'type'  => 'global',
                'field' => 'input',
                'name'  => 'Слоган сайта',
                'key'   => 'site_slogan',
                'val'   => 'Товары для дома - у нас есть всё!',
            ],
            [
                'type'  => 'global',
                'field' => 'textarea',
                'name'  => 'Meta Title',
                'key'   => 'site_meta_t',
                'val'   => 'Постельное белье, покрывала, подушки, одеяла, скатерти, салфетки, гобелены',
            ],
            [
                'type'  => 'global',
                'field' => 'textarea',
                'name'  => 'Meta Description',
                'key'   => 'site_meta_d',
                'val'   => 'Постельное белье, покрывала, подушки, одеяла, скатерти, салфетки, гобелены',
            ],
            [
                'type'  => 'global',
                'field' => 'textarea',
                'name'  => 'Meta Keywords',
                'key'   => 'site_meta_k',
                'val'   => 'Постельное белье, покрывала, подушки, одеяла, скатерти, салфетки, гобелены',
            ],
            [
                'type'  => 'global',
                'field' => 'input',
                'name'  => 'Копирайт',
                'key'   => 'site_copyright',
                'val'   => '© 2016, Интернет-магазин Три Слона. Все права защищены.',
            ],

            // Contacts
            [
                'type'  => 'contact',
                'field' => 'input',
                'name'  => 'Адрес',
                'key'   => 'address',
                'val'   => 'г. Киев',
            ],
            [
                'type'  => 'contact',
                'field' => 'input',
                'name'  => 'Адрес для Google Maps',
                'key'   => 'address_map',
                'val'   => 'Bankwest 3/345 George Street Sydney NSW 2000 Австралия',
            ],
            [
                'type'  => 'contact',
                'field' => 'textarea',
                'name'  => 'График работы',
                'key'   => 'business_hours',
                'val'   => 'С понедельника по пятницу с 10:00 до 19:00',
            ],
            [
                'type'  => 'contact',
                'field' => 'input',
                'name'  => 'Телефон 1',
                'key'   => 'phone_1',
                'val'   => '(098) 704-79-65',
            ],
            [
                'type'  => 'contact',
                'field' => 'input',
                'name'  => 'Телефон 2',
                'key'   => 'phone_2',
                'val'   => '(093) 909-65-35',
            ],
            [
                'type'  => 'contact',
                'field' => 'input',
                'name'  => 'Телефон 3',
                'key'   => 'phone_3',
                'val'   => '(095) 681-54-75',
            ],
            [
                'type'  => 'contact',
                'field' => 'input',
                'name'  => 'Email',
                'key'   => 'email',
                'val'   => 'trislona.com.ua@gmail.com',
            ],
            [
                'type'  => 'contact',
                'field' => 'input',
                'name'  => 'Skype',
                'key'   => 'skype',
                'val'   => 'trislona',
            ],
            [
                'type'  => 'contact',
                'field' => 'input',
                'name'  => 'Вконтакте',
                'key'   => 'vk_link',
                'val'   => '',
            ],
            [
                'type'  => 'contact',
                'field' => 'input',
                'name'  => 'Facebook',
                'key'   => 'fb_link',
                'val'   => '',
            ],
            [
                'type'  => 'contact',
                'field' => 'input',
                'name'  => 'Twitter',
                'key'   => 'tw_link',
                'val'   => '',
            ],

        ]);
    }
}
