<?php

use Faker\Factory;
use App\Models\Shop\Product;
use App\Models\Shop\Category;
use Illuminate\Database\Seeder;

class ShopProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::truncate();

        $this->fake = Factory::create();

        $product = [];
        $categories = Category::where('depth', 2)->get(); // Only for 3 level categories
        foreach ($categories as $category) {              // For all categories last level
            for ($i = 0; $i < mt_rand(2, 5); $i++) {      // From 1 to 3 products in category
                $title = $this->fake->sentence(mt_rand(3, 7));
                $product[] = [
                    'category_id' => $category->id,
                    'currency_id' => mt_rand(1, 2),
                    'brand_id'    => mt_rand(1, 5),
                    'slug'        => str_slug($title),
                    'title'       => $title,
                    'preview'     => $this->fake->paragraph(mt_rand(3, 6)),
                    'content'     => join(PHP_EOL.PHP_EOL, $this->fake->paragraphs(mt_rand(5, 8))),
                    'price'       => mt_rand(1, 15000),
                    'is_hit'      => mt_rand(0, 1),
                    'is_hot'      => mt_rand(0, 1),
                    'is_action'   => mt_rand(0, 1),
                    'is_enabled'  => mt_rand(0, 1),
                    'image'       => 'http://lorempixel.com/640/480/?' . uniqid(),
                    'meta_k'      => $title,
                    'meta_d'      => $title,
                    'created_at'  => date('Y-m-d H:i:s', time()),
                    'updated_at'  => date('Y-m-d H:i:s', time()),
                ];
            }
        }
        Product::insert($product);

        $pivot = [];
        foreach (Product::all() as $product) {
            $pivot[] = [
                'product_id'  => $product->id,
                'category_id' => $product->category_id,
            ];
        }
        DB::table('shop_product_category')->insert($pivot);
    }
}
