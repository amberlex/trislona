<?php

use Illuminate\Database\Seeder;
use App\Models\Shop\Currency;

class ShopCurrencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Currency::truncate();

        Currency::insert([
            [
                'code'       => 'UAH',
                'name'       => 'Гривна',
                'symbol'     => 'грн',
                'rate'       => '1',
                'symbol_pos' => 'right',
                'is_main'    => 1,
                'is_enabled' => 1,
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'code'       => 'USD',
                'name'       => 'Доллар США',
                'symbol'     => '$',
                'rate'       => '0.0375',
                'symbol_pos' => 'left',
                'is_main'    => 0,
                'is_enabled' => 1,
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'code'       => 'EUR',
                'name'       => 'Евро',
                'symbol'     => '€',
                'rate'       => '1.000',
                'symbol_pos' => 'right',
                'is_main'    => 0,
                'is_enabled' => 0,
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
            [
                'code'       => 'RUB',
                'name'       => 'Рубль',
                'symbol'     => 'руб',
                'rate'       => '1.000',
                'symbol_pos' => 'right',
                'is_main'    => 0,
                'is_enabled' => 0,
                'created_at' => date('Y-m-d H:i:s', time()),
                'updated_at' => date('Y-m-d H:i:s', time()),
            ],
        ]);
    }
}
