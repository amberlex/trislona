<?php

use Illuminate\Database\Seeder;
use App\Models\Publications\Rubric;

class PublicationsRubricTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Rubric::truncate();

        Rubric::insert([
            [
                'slug'        => 'aktsii',
                'title'       => 'Акции',
                'description' => 'Описание рубрики Акции',
                'image'       => 'http://lorempixel.com/640/480/?' . uniqid(),
                'icon'        => 'fa-asterisk',
                'meta_k'      => 'Акции',
                'meta_d'      => 'Акции',
                'created_at'  => date('Y-m-d H:i:s', time()),
                'updated_at'  => date('Y-m-d H:i:s', time()),
            ],
            [
                'slug'        => 'novosti',
                'title'       => 'Новости',
                'description' => 'Описание рубрики Новости',
                'image'       => 'http://lorempixel.com/640/480/?' . uniqid(),
                'icon'        => 'fa-newspaper-o',
                'meta_k'      => 'Новости',
                'meta_d'      => 'Новости',
                'created_at'  => date('Y-m-d H:i:s', time()),
                'updated_at'  => date('Y-m-d H:i:s', time()),
            ],
            [
                'slug'        => 'stati',
                'title'       => 'Статьи',
                'description' => 'Описание рубрики Статьи',
                'image'       => 'http://lorempixel.com/640/480/?' . uniqid(),
                'icon'        => 'fa-file-text-o',
                'meta_k'      => 'Статьи',
                'meta_d'      => 'Статьи',
                'created_at'  => date('Y-m-d H:i:s', time()),
                'updated_at'  => date('Y-m-d H:i:s', time()),
            ],
        ]);
    }
}
