<?php

$base_url = $app->runningInConsole() ? config('app.url') : url('/');

return [
    'brand_image' => [
        'path' => base_path() . '/public/uploads/shop/brand/',
        'url' => $base_url . '/public/uploads/shop/brand/',
        'thumbnails' => [
            'admin' => [
                'sub_dir' => 'admin/',
                'max_width' => 180,
                'max_height' => 135,
                'jpeg_quality' => 75,
            ],
            'th' => [
                'sub_dir' => 'th/',
                'max_width' => 240,
                'max_height' => 180,
                'jpeg_quality' => 75,
            ],
        ],
    ],
    'category_image' => [
        'path' => base_path() . '/public/uploads/shop/category/',
        'url' => $base_url . '/public/uploads/shop/category/',
        'thumbnails' => [
            'admin' => [
                'sub_dir' => 'admin/',
                'max_width' => 180,
                'max_height' => 135,
                'jpeg_quality' => 75,
            ],
            'th' => [
                'sub_dir' => 'th/',
                'max_width' => 640,
                'max_height' => 480,
                'jpeg_quality' => 75,
            ],
        ],
    ],
    'product_image' => [
        'path' => base_path() . '/public/uploads/shop/product/',
        'url' => $base_url . '/public/uploads/shop/product/',
        'thumbnails' => [
            'admin' => [
                'sub_dir' => 'admin/',
                'max_width' => 180,
                'max_height' => 135,
                'jpeg_quality' => 75,
            ],
            'th' => [
                'sub_dir' => 'th/',
                'max_width' => 640,
                'max_height' => 480,
                'jpeg_quality' => 75,
            ],
        ],
    ],
];