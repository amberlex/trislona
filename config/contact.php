<?php

return [
    'mail_name' => env('MAIL_NAME'),
    'mail_from' => env('MAIL_FROM'),
    'mail_contact' => env('MAIL_CONTACT'),
];
