<?php

$base_url = $app->runningInConsole() ? config('app.url') : url('/');

return [
    'rubric_image' => [
        'path' => base_path() . '/public/uploads/publications/rubric/',
        'url' => $base_url . '/public/uploads/publications/rubric/',
        'thumbnails' => [
            'admin' => [
                'sub_dir' => 'admin/',
                'max_width' => 180,
                'max_height' => 135,
                'jpeg_quality' => 75,
            ],
            'th' => [
                'sub_dir' => 'th/',
                'max_width' => 640,
                'max_height' => 480,
                'jpeg_quality' => 75,
            ],
        ],
    ],
    'article_image' => [
        'path' => base_path() . '/public/uploads/publications/article/',
        'url' => $base_url . '/public/uploads/publications/article/',
        'thumbnails' => [
            'admin' => [
                'sub_dir' => 'admin/',
                'max_width' => 180,
                'max_height' => 135,
                'jpeg_quality' => 75,
            ],
            'th' => [
                'sub_dir' => 'th/',
                'max_width' => 640,
                'max_height' => 480,
                'jpeg_quality' => 75,
            ],
        ],
    ],
];