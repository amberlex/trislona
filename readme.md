# Shop trislona

Для возвращения базы данных в исходное состояние такой URL:

`http://trislona.kosmopoisk.ks.ua/seed`

### Based on Laravel PHP Framework

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

### Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

###Install project

To create the `trislona` rpoject run: 

`git clone git@bitbucket.org:amberlex/trislona.git` 

`cd trislona` 

Get the vendors

`composer install` 

Create the new `.env` file from `.env.example` 

Generate the application key 

`php artisan key:generate` 

Create the Database and set values in the `.env` file for: 
``` 
DB_HOST=localhost
DB_DATABASE=trislona
DB_USERNAME=null
DB_PASSWORD=null
``` 
Create the tables 

`php artisan migrate` 

To put the random data into the database

`php artisan db:seed` 

The following is to be added:

login: `admin@example.com` password `1q2w3e4r` 

login: `user@example.com` password `1q2w3e4r` 

To test mails you need to be registered on `https://mailtrap.io` and set values in the `.env` file for:
``` 
MAIL_HOST=mailtrap.io 
MAIL_USERNAME=null 
MAIL_PASSWORD=null
```