<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        $router->model('pages', \App\Models\Page::class);

        $router->model('settings', \App\Models\Settings::class);

        $router->model('rubrics',  \App\Models\Publications\Rubric::class);
        $router->model('articles', \App\Models\Publications\Article::class);

        $router->model('currencies', \App\Models\Shop\Currency::class);
        $router->model('brands',     \App\Models\Shop\Brand::class);
        $router->model('categories', \App\Models\Shop\Category::class);
        $router->model('products',   \App\Models\Shop\Product::class);

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
