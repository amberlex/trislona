<?php

namespace App\Widgets;

use App\Models\Shop\Product;
use Arrilot\Widgets\AbstractWidget;

class ProductOnHomePage extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $hits = Product::where('is_hit', 1)
            ->where('is_enabled', 1)
            ->where('image', '!=', '')
            ->take(6)
            ->get();

        $hots = Product::where('is_hot', 1)
            ->where('is_enabled', 1)
            ->where('image', '!=', '')
            ->take(6)
            ->get();

        $actions = Product::where('is_action', 1)
            ->where('is_enabled', 1)
            ->where('image', '!=', '')
            ->take(6)
            ->get();

        return view('widgets.product_on_home_page', [
            'hits'    => $hits,
            'hots'    => $hots,
            'actions' => $actions,
        ]);
    }
}