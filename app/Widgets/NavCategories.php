<?php

namespace App\Widgets;

use App\Models\Shop\Category;
use App\Models\Shop\Product;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Http\Request;

class NavCategories extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'view' => 'nav_pages'
    ];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run(Request $request)
    {
        $category = $request->segment(1) == 'product'
            ? Product::where('slug', $request->segment(2))->first()->category
            : Category::where('slug', $request->segment(2))->first();

        if ($category->depth == 1) {
            $category = $category->parent;
        }
        if ($category->depth == 2) {
            $category = $category->parent->parent;
        }

        $categories = $category->getDescendants(['slug', 'title', 'depth'])
            ->toArray();

        return view('widgets.nav_categories', [
            'title'      => $category->title,
            'categories' => $categories,
        ]);
    }
}