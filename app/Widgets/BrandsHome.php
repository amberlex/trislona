<?php

namespace App\Widgets;

use App\Models\Shop\Brand;
use Arrilot\Widgets\AbstractWidget;

class BrandsHome extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $brands = Brand::where('image', '!=', '')->where('id', '>', 1)->take(72)->get();

        return view("widgets.brands_home", [
            'brands' => $brands,
        ]);
    }
}