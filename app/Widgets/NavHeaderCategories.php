<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Facades\DB;

class NavHeaderCategories extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $categories = DB::table('shop_categories')
            ->select('title', 'slug')
            ->where('id', '>', 1)
            ->where('depth', 0)
            ->orderBy('lft')
            ->get();

        return view("widgets.nav_header_categories", [
            'categories' => $categories,
        ]);
    }
}