<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Facades\DB;

class NavPages extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'view' => 'nav_pages'
    ];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $rubrics = DB::table('publications_rubrics')
            ->select('title', 'slug', 'icon')
            ->get();

        $pages = DB::table('pages')
            ->select('title', 'slug', 'icon')
            ->where('is_enabled', '=', 1)
            ->get();

        return view('widgets.' . $this->config['view'], [
            'rubrics' => $rubrics,
            'pages'   => $pages,
        ]);
    }
}