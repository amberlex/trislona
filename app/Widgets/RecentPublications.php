<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Models\Publications\Article;

class RecentPublications extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'limit' => 4
    ];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $articles = Article::with('rubric')
            ->whereIsEnabled(1)
            ->published()
            ->take($this->config['limit'])
            ->orderBy('published_at', 'desc')
            ->get();

        return view('widgets.recent_publications', [
            'articles' => $articles,
        ]);
    }
}