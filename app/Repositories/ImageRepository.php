<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Intervention\Image\ImageManager;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;

class ImageRepository
{
    /**
     * @var string Filename for image
     */
    protected $newFileName;

    /**
     * @var void Instance of Image Manager
     */
    protected $manager;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Model Current model
     */
    protected $model;

    /**
     * @var array Current config from model
     */
    protected $config;

    /**
     * @var UploadedFile
     */
    protected $requestFile;

    /**
     * Init
     *
     * @param string $key  Path to image in config file
     * @param Model $model
     * @param Request|null $request
     */
    public function init($key, Model $model, Request $request = null)
    {
        if ($request !== null) {
            $this->request = $request;
            $this->requestFile = $request->file('image');
        }

        $this->model   = $model;
        $this->config  = config($key);
        $this->manager = new ImageManager();
    }

    /**
     * Validate uploading image
     *
     * @return \Illuminate\Validation\Validator
     */
    public function validator()
    {
        return \Validator::make($this->request->file(), [
            'image' => 'image|mimes:jpeg,jpg,png,gif',
        ]);
    }

    /**
     * Upload image and save filename to DB
     */
    public function uploadImage()
    {
        $this->setNewFileName();
        $this->makeDirectories();

        $this->deleteFiles(); // Delete previous images
        $this->uploadFiles(); // Upload new images

        // Save changes to DB
        $this->model->image = $this->newFileName;
        $this->model->save();
    }

    /**
     * Delete image and thumbnails files, save empty value to DB
     */
    public function deleteImage()
    {
        $this->deleteFiles(); // Delete previous images

        // Save changes to DB
        $this->model->image = null;
        $this->model->save();
    }

    /**
     * Upload all files of images for record
     */
    public function uploadFiles()
    {
        // Original Image
        $this->manager
            ->make($this->requestFile)
            ->save($this->config['path'] . $this->newFileName);

        // Thumbnails
        foreach ($this->config['thumbnails'] as $type => $thumbnail) {
            if ($type == 'admin') {
                $this->manager
                    ->make($this->requestFile)
                    ->fit($thumbnail['max_width'], $thumbnail['max_height'])
                    ->save($this->config['path'] . $thumbnail['sub_dir'] . $this->newFileName);
            } elseif ($type == 'th') {
                $this->manager
                    ->make($this->requestFile)

                    ->widen($thumbnail['max_width'], function ($constraint) {
                        $constraint->upsize();
                    })

//                    ->resize($thumbnail['max_width'], null, function ($constraint) {
//                        $constraint->aspectRatio();
//                    })

                    ->save($this->config['path'] . $thumbnail['sub_dir'] . $this->newFileName);
            }
        }
    }

    /**
     * Delete all files of images for record
     */
    public function deleteFiles()
    {
        // Original Image
        File::delete($this->config['path'] . $this->model->image);

        // Thumbnails
        foreach ($this->config['thumbnails'] as $thumbnail) {
            File::delete($this->config['path'] . $thumbnail['sub_dir'] . $this->model->image);
        }
    }


    /**
     * Generate file name for image
     */
    protected function setNewFileName()
    {
        $this->newFileName = time() . uniqid('_') . '.jpg';
    }

    /**
     * Make directories for thumbnails if they not exists
     */
    protected function makeDirectories()
    {
        foreach ($this->config['thumbnails'] as $thumbnail) {
            if ( ! File::isDirectory($this->config['path'] . $thumbnail['sub_dir'])) {
                File::makeDirectory($this->config['path'] . $thumbnail['sub_dir']);
            }
        }
    }
}