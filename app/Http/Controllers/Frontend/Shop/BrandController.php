<?php

namespace App\Http\Controllers\Frontend\Shop;

use App\Models\Shop\Brand;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BrandController extends Controller
{
    /**
     * Display Brands
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $brands = Brand::where('id', '>', 1)->get();

        $breadcrumbs[] = ['label' => 'Бренды'];

        return view('frontend.shop.brand.index', [
            'breadcrumbs' => $breadcrumbs,
            'brands'      => $brands,
        ]);
    }


    public function show($slug)
    {
        $brand = Brand::whereSlug($slug)->where('id', '>', 1)->firstOrFail();

        $breadcrumbs[] = ['label' => 'Бренды', 'url' => route('shop.brands')];
        $breadcrumbs[] = ['label' => $brand->title];

        return view('frontend.shop.brand.show', [
            'breadcrumbs' => $breadcrumbs,
            'brand'       => $brand,
        ]);
    }
}
