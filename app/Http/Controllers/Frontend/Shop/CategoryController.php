<?php

namespace App\Http\Controllers\Frontend\Shop;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Shop\Category;

class CategoryController extends Controller
{
    /**
     * Display Categories
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $categories = Category::limitDepth(0)
            ->where('id', '>', 1)
            ->orderBy('lft')
            ->get(['slug', 'image', 'title', 'description']);

        $breadcrumbs = [
            ['label' => 'Категории'],
        ];

        return view('frontend.shop.category.index', [
            'breadcrumbs' => $breadcrumbs,
            'categories'  => $categories,
        ]);
    }


    /**
     * Display category
     *
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($slug)
    {
        $category = Category::whereSlug($slug)->where('id', '>', 1)->firstOrFail();

        $descendants = $category->getDescendants(3)->toHierarchy();

        $ancestors = $category->getAncestors(['slug', 'title']);
        $breadcrumbs[] = ['label' => 'Категории', 'url' => route('shop.categories')];
        foreach($ancestors as $ancestor) {
            $breadcrumbs[] = [
                'label' => $ancestor->title,
                'url' => route('shop.category', ['slug' => $ancestor->slug])
            ];
        }
        $breadcrumbs[] = ['label' => $category->title];

        $products = $category->products()
            ->where('is_enabled', 1)
            ->get();

        return view('frontend.shop.category.show', [
            'breadcrumbs' => $breadcrumbs,
            'category'    => $category,
            'descendants' => $descendants,
            'products'    => $products,
        ]);
    }

}
