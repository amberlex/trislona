<?php

namespace App\Http\Controllers\Frontend\Shop;

use App\Models\Shop\Product;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    /**
     * Return the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($slug)
    {
        $product = Product::whereSlug($slug)
            ->whereIsEnabled(1)
            ->firstOrFail();

        $ancestors = $product->category->getAncestorsAndSelf(['slug', 'title']);
        $breadcrumbs[] = ['label' => 'Категории', 'url' => route('shop.categories')];
        foreach($ancestors as $ancestor) {
            $breadcrumbs[] = [
                'label' => $ancestor->title,
                'url' => route('shop.category', ['slug' => $ancestor->slug])
            ];
        }
        $breadcrumbs[] = ['label' => $product->title];

        return view('frontend.shop.product.show', [
            'breadcrumbs' => $breadcrumbs,
            'product'     => $product,
        ]);
    }
}
