<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Page;

class PageController extends Controller
{
    public function index($slug)
    {
        $page = Page::whereSlug($slug)
            ->whereIsEnabled(1)
            ->firstOrFail();

        $breadcrumbs = [
            ['label' => $page->title],
        ];

        return view('frontend.page.index', [
            'breadcrumbs' => $breadcrumbs,
            'page'        => $page,
        ]);
    }
}
