<?php

namespace App\Http\Controllers\Frontend\Publications;

use App\Models\Publications\Article;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Publications\Rubric;

class RubricController extends Controller
{
    /**
     * Display Publications of Rubrics or Rubric
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $rubrics = Rubric::all();

        $breadcrumbs[] = ['label' => 'Рубрики'];

        return view('frontend.publications.rubric.index', [
            'breadcrumbs' => $breadcrumbs,
            'rubrics'     => $rubrics,
        ]);
    }

    /**
     * Return the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($slug)
    {
        $rubric = Rubric::whereSlug($slug)->firstOrFail();

        $articles = $rubric->articles()
            ->whereIsEnabled(1)
            ->published()
            ->orderBy('published_at', 'desc')
            ->get();

        $breadcrumbs[] = ['label' => $rubric->title];

        return view('frontend.publications.rubric.show', [
            'breadcrumbs' => $breadcrumbs,
            'rubric'      => $rubric,
            'articles'    => $articles,
        ]);
    }
}
