<?php

namespace App\Http\Controllers\Frontend\Publications;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Publications\Article;
use Illuminate\Support\Facades\Route;

class ArticleController extends Controller
{
    /**
     * Display Publications of Articles or Article
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $articles = Article::with('rubric')
            ->whereIsEnabled(1)
            ->published()
            ->orderBy('published_at', 'desc')
            ->get();

        $breadcrumbs[] = ['label' => 'Публикации'];

        return view('frontend.publications.article.index', [
            'breadcrumbs' => $breadcrumbs,
            'articles'    => $articles
        ]);
    }

    /**
     * Return the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($slug)
    {
        $article = Article::whereSlug($slug)
            ->whereIsEnabled(1)
            ->published()
            ->firstOrFail();

        $breadcrumbs = [
            [
                'label' => $article->rubric->title,
                'url'   => route('publications.rubric', $article->rubric->slug)
            ],
            ['label' => $article->title],
        ];

        return view('frontend.publications.article.show', [
            'breadcrumbs' => $breadcrumbs,
            'article'     => $article,
        ]);
    }
}
