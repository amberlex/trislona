<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Requests;

class HomeController extends Controller
{
    public function index()
    {
        return view('frontend.home.index');
    }
}
