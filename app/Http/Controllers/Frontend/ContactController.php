<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\ContactFormRequest;

class ContactController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $breadcrumbs = [
            ['label' => 'Контакты'],
        ];

        return view('frontend.contact.create',['breadcrumbs' => $breadcrumbs]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param ContactFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ContactFormRequest $request)
    {
        $data = $request->only('name', 'email');
        $data['messageLines'] = explode("\n", $request->get('message'));

        Mail::send('emails.contact', $data, function ($message) use ($data) {
            $message->subject('Обратная связь: ' . $data['name'])
                ->to(config('contact.mail_contact'))
                ->from(config('contact.mail_from'), config('contact.mail_name'))
                ->replyTo($data['email']);
        });

        return back()->withSuccess("Спасибо за ваше сообщение.");
    }
}