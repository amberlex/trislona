<?php

namespace App\Http\Controllers\Backend;

use App\Models\Settings;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    /**
     * SettingsController constructor.
     *
     * For highlight backend menu items.
     */
    public function __construct()
    {
        // Set global var for a views
        view()->share('module', 'settings');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = Settings::all();

        return view('backend.settings.index', ['settings' => $settings]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Settings $setting
     * @return \Illuminate\Http\Response
     */
    public function edit(Settings $setting)
    {
        return view('backend.settings.edit', ['setting' => $setting]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Settings $setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Settings $setting)
    {
        $setting->update($request->all());

        return redirect()
            ->route('backend.settings.index')
            ->withSuccess('Parameter saved.');
    }
}
