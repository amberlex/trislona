<?php

namespace App\Http\Controllers\Backend;

use App\Models\Page;
use App\Http\Requests\PageFormRequest;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    /**
     * PageController constructor.
     *
     * For highlight backend menu items.
     */
    public function __construct()
    {
        // Set global var for a views
        view()->share('module', 'pages');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        session()->put('backUrl', url()->full());
        $pages = Page::all();

        return view('backend.page.index', ['pages' => $pages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.page.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PageFormRequest $request
     * @param Page $page
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(PageFormRequest $request, Page $page)
    {
        $page = $page->create($request->all());

        return $request->get('save_and_exit')
            ? redirect()->route('backend.pages.index')
            : redirect()->route('backend.pages.edit', [$page]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Page $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Page $page)
    {
        return view('backend.page.edit', ['page' => $page]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PageFormRequest $request
     * @param Page $page
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(PageFormRequest $request, Page $page)
    {
        $page->update($request->all());

        return $request->get('save_and_exit')
            ? redirect()->route('backend.pages.index')
            : redirect()->route('backend.pages.edit', [$page]);
    }

    /**
     * Show the form for confirmation delete resource.
     *
     * @param Page $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function confirm(Page $page)
    {
        return view('backend.page.confirm', ['page' => $page]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Page $page
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Page $page)
    {
        $page->delete();

        return redirect()
            ->route('backend.pages.index')
            ->withSuccess('Page deleted.');
    }
}
