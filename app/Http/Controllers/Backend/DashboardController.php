<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * DashboardController constructor.
     *
     * For highlight backend menu items.
     */
    public function __construct()
    {
        // Set global var for a views
        view()->share('module', 'dashboard');
    }

    /**
     * Display a Dashboard page.
     */
    public function index()
    {
        return view('backend.dashboard.index');
    }
}
