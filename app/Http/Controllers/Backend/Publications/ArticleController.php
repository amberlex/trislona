<?php

namespace App\Http\Controllers\Backend\Publications;

use App\Models\Publications\Rubric;
use App\Models\Publications\Article;
use App\Http\Requests\ArticleFormRequest;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\ImageRepository;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * ArticleController constructor.
     *
     * For highlight backend menu items.
     */
    public function __construct()
    {
        // Set global var for a views
        view()->share('module', 'publications');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        session()->put('backUrl', url()->full());

        $articles = Article::with('rubric')
            ->latest('created_at')
            ->paginate(12);

        return view('backend.publications.article.index', ['articles' => $articles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rubrics = Rubric::lists('title', 'id');

        return view('backend.publications.article.create', ['rubrics' => $rubrics]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ArticleFormRequest $request
     * @param Article $article
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(ArticleFormRequest $request, Article $article)
    {
        $article = $article->create($request->all());

        return $request->get('save_and_exit')
            ? redirect()->route('backend.articles.index')
            : redirect()->route('backend.articles.edit', [$article]);
            //->withSuccess('New Article Created.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Article $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        $rubrics = Rubric::lists('title', 'id');

        return view('backend.publications.article.edit', [
            'rubrics' => $rubrics,
            'article' => $article,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ArticleFormRequest $request
     * @param  Article $article
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleFormRequest $request, Article $article)
    {
        $article->update($request->all());

        return $request->get('save_and_exit')
            ? redirect()->route('backend.articles.index')
            : redirect()->route('backend.articles.edit', [$article]);
            //->withSuccess('Article saved.');
    }

    /**
     * @param Request $request
     * @param Article $article
     * @param ImageRepository $imageRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function imgUpload(Request $request, Article $article, ImageRepository $imageRepository)
    {
        if ($request->ajax()) {
            $imageRepository->init('publications.article_image', $article, $request);

            $validator = $imageRepository->validator();
            if ($validator->fails()) {
                return response()->json([
                    'success' => false,
                    'message' => $validator->messages()->first(),
                ], 400);
            }

            $imageRepository->uploadImage();

            return response()->json([
                'success' => true,
                'image'   => $article->admin_image,
            ], 200);
        } else {
            return response('Bad Request', 400);
        }
    }

    /**
     * @param Request $request
     * @param Article $article
     * @param ImageRepository $imageRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function imgDelete(Request $request, Article $article, ImageRepository $imageRepository)
    {
        if ($request->ajax()) {
            $imageRepository->init('publications.article_image', $article, $request);
            $imageRepository->deleteImage();

            return response()->json([
                'success' => true,
            ], 200);
        } else {
            return response('Bad Request', 400);
        }
    }

    /**
     * Show the form for confirmation delete resource.
     *
     * @param Article $article
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function confirm(Article $article)
    {
        return view('backend.publications.article.confirm', ['article' => $article]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Article $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article, ImageRepository $imageRepository)
    {
        $article->delete();

        $imageRepository->init('publications.article_image', $article);
        $imageRepository->deleteFiles();

        return redirect()
            ->route('backend.articles.index')
            ->withSuccess('Article deleted.');
    }
}
