<?php

namespace App\Http\Controllers\Backend\Publications;

use App\Models\Publications\Rubric;
use App\Http\Requests\RubricFormRequest;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\ImageRepository;
use Illuminate\Http\Request;

class RubricController extends Controller
{
    /**
     * RubricController constructor.
     *
     * For highlight backend menu items.
     */
    public function __construct()
    {
        // Set global var for a views
        view()->share('module', 'publications');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        session()->put('backUrl', url()->full());
        $rubrics = Rubric::all();

        return view('backend.publications.rubric.index', ['rubrics' => $rubrics]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.publications.rubric.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RubricFormRequest $request
     * @param Rubric $rubric
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(RubricFormRequest $request, Rubric $rubric)
    {
        $rubric = $rubric->create($request->all());

        return $request->get('save_and_exit')
            ? redirect()->route('backend.rubrics.index')
            : redirect()->route('backend.rubrics.edit', [$rubric]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Rubric $rubric
     * @return \Illuminate\Http\Response
     */
    public function edit(Rubric $rubric)
    {
        return view('backend.publications.rubric.edit', ['rubric' => $rubric]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  RubricFormRequest $request
     * @param  Rubric $rubric
     * @return \Illuminate\Http\Response
     */
    public function update(RubricFormRequest $request, Rubric $rubric)
    {
        $rubric->update($request->all());

        return $request->get('save_and_exit')
            ? redirect()->route('backend.rubrics.index')
            : redirect()->route('backend.rubrics.edit', [$rubric]);
    }

    /**
     * @param Request $request
     * @param Rubric $rubric
     * @param ImageRepository $imageRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function imgUpload(Request $request, Rubric $rubric, ImageRepository $imageRepository)
    {
        if ($request->ajax()) {
            $imageRepository->init('publications.rubric_image', $rubric, $request);

            $validator = $imageRepository->validator();
            if ($validator->fails()) {
                return response()->json([
                    'success' => false,
                    'message' => $validator->messages()->first(),
                ], 400);
            }

            $imageRepository->uploadImage();

            return response()->json([
                'success' => true,
                'image'   => $rubric->admin_image,
            ], 200);
        } else {
            return response('Bad Request', 400);
        }
    }

    /**
     * @param Request $request
     * @param Rubric $rubric
     * @param ImageRepository $imageRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function imgDelete(Request $request, Rubric $rubric, ImageRepository $imageRepository)
    {
        if ($request->ajax()) {
            $imageRepository->init('publications.rubric_image', $rubric, $request);
            $imageRepository->deleteImage();

            return response()->json([
                'success' => true,
            ], 200);
        } else {
            return response('Bad Request', 400);
        }
    }

    /**
     * Show the form for confirmation delete resource.
     *
     * @param Rubric $rubric
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function confirm(Rubric $rubric)
    {
        return view('backend.publications.rubric.confirm', ['rubric' => $rubric]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Rubric $rubric
     * @param  ImageRepository $imageRepository
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rubric $rubric, ImageRepository $imageRepository)
    {
        $rubric->delete();

        $imageRepository->init('publications.rubric_image', $rubric);
        $imageRepository->deleteFiles();

        return redirect()
            ->route('backend.rubrics.index')
            ->withSuccess('Rubric deleted.');
    }
}
