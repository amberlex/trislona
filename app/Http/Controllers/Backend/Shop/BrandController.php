<?php

namespace App\Http\Controllers\Backend\Shop;

use App\Models\Shop\Brand;
use App\Models\Country;
use App\Http\Requests\BrandFormRequest;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\ImageRepository;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    /**
     * BrandController constructor.
     *
     * For highlight backend menu items.
     */
    public function __construct()
    {
        // Set global var for a views
        view()->share('module', 'shop');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        session()->put('backUrl', url()->full());
        $brands = Brand::with('country')->orderBy('title')->get();

        return view('backend.shop.brand.index', ['brands' => $brands]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::lists('title', 'id');

        return view('backend.shop.brand.create', ['countries' => $countries]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BrandFormRequest $request
     * @param Brand $brand
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(BrandFormRequest $request, Brand $brand)
    {
        $brand = $brand->create($request->all());

        return $request->get('save_and_exit')
            ? redirect()->route('backend.brands.index')
            : redirect()->route('backend.brands.edit', [$brand]);
            //->withSuccess('Бренд создан.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Brand $brand
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Brand $brand)
    {
        $countries = Country::lists('title', 'id');

        return view('backend.shop.brand.edit', ['brand' => $brand, 'countries' => $countries]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BrandFormRequest $request
     * @param Brand $brand
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(BrandFormRequest $request, Brand $brand)
    {
        $brand->update($request->all());

        return $request->get('save_and_exit')
            ? redirect()->route('backend.brands.index')
            : redirect()->route('backend.brands.edit', [$brand]);
            //->withSuccess('Изменения сохранены.');
    }

    /**
     * @param Request $request
     * @param Brand $brand
     * @param ImageRepository $imageRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function imgUpload(Request $request, Brand $brand, ImageRepository $imageRepository)
    {
        if ($request->ajax()) {
            $imageRepository->init('shop.brand_image', $brand, $request);

            $validator = $imageRepository->validator();
            if ($validator->fails()) {
                return response()->json([
                    'success' => false,
                    'message' => $validator->messages()->first(),
                ], 400);
            }

            $imageRepository->uploadImage();

            return response()->json([
                'success' => true,
                'image'   => $brand->admin_image,
            ], 200);
        } else {
            return response('Bad Request', 400);
        }
    }

    /**
     * @param Request $request
     * @param Brand $brand
     * @param ImageRepository $imageRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function imgDelete(Request $request, Brand $brand, ImageRepository $imageRepository)
    {
        if ($request->ajax()) {
            $imageRepository->init('shop.brand_image', $brand, $request);
            $imageRepository->deleteImage();

            return response()->json([
                'success' => true,
            ], 200);
        } else {
            return response('Bad Request', 400);
        }
    }

    /**
     * Show the form for confirmation delete resource.
     *
     * @param Brand $brand
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function confirm(Brand $brand)
    {
        return view('backend.shop.brand.confirm', ['brand' => $brand]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ImageRepository $imageRepository
     * @param Brand $brand
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Brand $brand, ImageRepository $imageRepository)
    {
        $brand->delete();

        $imageRepository->init('shop.brand_image', $brand);
        $imageRepository->deleteFiles();

        return redirect()
            ->route('backend.brands.index')
            ->withSuccess('Бренд удален.');
    }
}
