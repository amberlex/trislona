<?php

namespace App\Http\Controllers\Backend\Shop;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Shop\Category;
use App\Http\Requests\CategoryFormRequest;
use Baum\MoveNotPossibleException;
use App\Repositories\ImageRepository;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * @var Category
     */
    protected $category;

    /**
     * CategoryController constructor.
     *
     * @param Category $category
     * For highlight backend menu items.
     */
    public function __construct(Category $category)
    {
        // Set global var for a views
        view()->share('module', 'shop');

        $this->category = $category;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        session()->put('backUrl', url()->full());

        $categories = $this->category->roots()
            ->where('id', '>', 1)
            ->get(['id', 'image', 'slug', 'title']);

        return view('backend.shop.category.index', [
            'categories' => $categories
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Category $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function descendants(Category $category)
    {
        session()->put('backUrl', url()->full());

        $categories  = $category->getRoots();

        $descendants = $category->getDescendants(['id', 'image', 'slug', 'title', 'depth']);

        return view('backend.shop.category.descendants', [
            'category_id' => $category->id,
            'categories'  => $categories,
            'descendants' => $descendants,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.shop.category.create', [
            'categoriesForSelect' => $this->category->getForSelect(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CategoryFormRequest $request
     * @param Category $category
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(CategoryFormRequest $request, Category $category)
    {
        $category = $category->create($request->all());
        $this->updateCategoryOrder($category, $request);
        $category->updatePath();

        return $request->get('save_and_exit')
            ? redirect(session('backUrl'))
            : redirect()->route('backend.categories.edit', [$category]);
            //->withSuccess('Категория создана.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Category $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('backend.shop.category.edit', [
            'categoriesForSelect' => $this->category->getForSelect(),
            'category'            => $category,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CategoryFormRequest $request
     * @param  Category $category
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryFormRequest $request, Category $category)
    {
        $categoryDepth = $category->depth;

        if ($response = $this->updateCategoryOrder($category, $request)) {
            return $response;
        }

        $category->update($request->all());
        $category->updatePath();

        // На третий уровень нельзя
        // Еесли спустились на первый, то необходимо обновить пути для детей узла
        // (они как раз были листьями когда узел был на втором уровне)
        if ($categoryDepth > $category->depth) {
            foreach ($category->leaves()->get() as $leaf) {
                $leaf->updatePath();
            }
        }

        return $request->get('save_and_exit')
            ? redirect(session('backUrl'))
            : redirect()->route('backend.categories.edit', [$category]);
            //->withSuccess('Изменения сохранены.');
    }

    /**
     * @param Request $request
     * @param Category $category
     * @param ImageRepository $imageRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function imgUpload(Request $request, Category $category, ImageRepository $imageRepository)
    {
        if ($request->ajax()) {
            $imageRepository->init('shop.category_image', $category, $request);

            $validator = $imageRepository->validator();
            if ($validator->fails()) {
                return response()->json([
                    'success' => false,
                    'message' => $validator->messages()->first(),
                ], 400);
            }

            $imageRepository->uploadImage();

            return response()->json([
                'success' => true,
                'image'   => $category->response_image,
            ], 200);
        } else {
            return response('Bad Request', 400);
        }
    }

    /**
     * @param Request $request
     * @param Category $category
     * @param ImageRepository $imageRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function imgDelete(Request $request, Category $category, ImageRepository $imageRepository)
    {
        if ($request->ajax()) {
            $imageRepository->init('shop.category_image', $category, $request);
            $imageRepository->deleteImage();

            return response()->json([
                'success' => true,
            ], 200);
        } else {
            return response('Bad Request', 400);
        }
    }

    /**
     * Show the form for confirmation delete resource.
     *
     * @param Category $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function confirm(Category $category)
    {
        return view('backend.shop.category.confirm', [
            'category' => $category,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  ImageRepository $imageRepository
     * @param  Category $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category, ImageRepository $imageRepository)
    {
        if ($category->id == 1) {
            return redirect(session('backUrl'))
                ->withErrors('Вы не можете удалить эту категорию.');
        }

        $category->moveProducts();
        $category->delete();

        $imageRepository->init('shop.category_image', $category);
        $imageRepository->deleteFiles();

        return redirect(session('backUrl'))
            ->withSuccess('Категория удалена');
    }


    //------------------------------------------------------------------------------------------------------------------
    // PROTECTED

    /**
     * @param Category $category
     * @param Request $request
     * @return $this
     */
    protected function updateCategoryOrder(Category $category, Request $request)
    {
        if ($request->has('order', 'order_category')) {
            try {
                $error = $category->updateOrder($request->input('order'), $request->input('order_category'));
                if ($error) {
                    return redirect(route('backend.categories.edit', $category->id))->withInput()->withErrors([
                        'error' => 'Невозможно переместить категорию на этот уровень.'
                    ]);
                }
            } catch (MoveNotPossibleException $e) {
                return redirect(route('backend.categories.edit', $category->id))->withInput()->withErrors([
                    'error' => 'Cannot make a page a child of itself.'
                ]);
            }
        }
    }
}
