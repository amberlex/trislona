<?php

namespace App\Http\Controllers\Backend\Shop;

use App\Models\Shop\Currency;
use App\Http\Requests\CurrencyFormRequest;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CurrencyController extends Controller
{
    /**
     * CurrencyController constructor.
     *
     * For highlight backend menu items.
     */
    public function __construct()
    {
        // Set global var for a views
        view()->share('module', 'settings');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        session()->put('backUrl', url()->full());
        $currencies = Currency::orderBy('is_main', 'desc')->get();

        return view('backend.shop.currency.index', ['currencies' => $currencies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.shop.currency.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CurrencyFormRequest $request
     * @param  Currency $currency
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(CurrencyFormRequest $request, Currency $currency)
    {
        $currency_main = Currency::where('is_main', 1)->first();
        $currency = $currency->create($request->all());
        $currency->saveIsMain($currency_main);

        return $request->get('save_and_exit')
            ? redirect()->route('backend.currencies.index')
            : redirect()->route('backend.currencies.edit', [$currency]);
            //->withSuccess('Новая валюта создана.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Currency $currency
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Currency $currency)
    {
        return view('backend.shop.currency.edit', ['currency' => $currency]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CurrencyFormRequest $request
     * @param  Currency $currency
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(CurrencyFormRequest $request, Currency $currency)
    {
        $currency_main = Currency::where('is_main', 1)->first();
        $currency->update($request->all());
        $currency->saveIsMain($currency_main);

        return $request->get('save_and_exit')
            ? redirect()->route('backend.currencies.index')
            : redirect()->route('backend.currencies.edit', [$currency]);
            //->withSuccess('Изменения сохранены.');
    }

    /**
     * Show the form for confirmation delete resource.
     *
     * @param Currency $currency
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function confirm(Currency $currency)
    {
        return view('backend.shop.currency.confirm', ['currency' => $currency]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Currency $currency
     * @return \Illuminate\Http\Response
     */
    public function destroy(Currency $currency)
    {
        if ($currency->is_main) {
            return redirect()
                ->route('backend.currencies.confirm', ['currency' => $currency])
                ->withErrors('Вы не можете удалить основную валюту.');
        }

        $currency->delete();

        return redirect()
            ->route('backend.currencies.index')
            ->withSuccess('Валюта удалена.');
    }
}
