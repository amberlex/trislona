<?php

namespace App\Http\Controllers\Backend\Shop;

use App\Models\Shop\Brand;
use App\Models\Shop\Category;
use App\Models\Shop\Currency;
use App\Models\Shop\Product;
use App\Http\Requests\ProductFormRequest;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\ImageRepository;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * @var Category
     */
    public $category;

    /**
     * @var Brand
     */
    public $brand;

    /**
     * @var Currency
     */
    public $currency;

    /**
     * ProductController constructor.
     *
     * ProductController constructor.
     * @param Category $category
     * @param Currency $currency
     * @param Brand $brand
     */
    public function __construct(Brand $brand, Category $category, Currency $currency)
    {
        // Set global var for a views
        view()->share('module', 'shop');

        $this->brand    = $brand;
        $this->category = $category;
        $this->currency = $currency;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        session()->put('backUrl', url()->full());

        $products = Product::with('category', 'currency', 'brand', 'brand.country')
            ->latest('created_at')
            ->paginate(12);

        return view('backend.shop.product.index', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('backend.shop.product.create', [
            'categoriesForSelect' => $this->category->getForSelect(),
            'currenciesForSelect' => $this->currency->getForSelect(),
            'brandsForSelect'     => $this->brand->lists('title', 'id'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductFormRequest $request
     * @param Product $product
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(ProductFormRequest $request, Product $product)
    {
        $product = $product->create($request->all());

        $this->syncCategories($product, $request->input('category_ids'));

        return $request->get('save_and_exit')
            ? redirect()->route('backend.products.index')
            : redirect()->route('backend.products.edit', [$product]);
            //->withSuccess('Новый товар добавлен.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Product $product
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Product $product)
    {
        return view('backend.shop.product.edit', [
            'categoriesForSelect' => $this->category->getForSelect(),
            'currenciesForSelect' => $this->currency->getForSelect(),
            'brandsForSelect'     => $this->brand->lists('title', 'id'),
            'product'             => $product,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ProductFormRequest $request
     * @param  Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductFormRequest $request, Product $product)
    {
        $product->update($request->all());

        $this->syncCategories($product, $request->input('category_ids'));

        return $request->get('save_and_exit')
            ? redirect()->route('backend.products.index')
            : redirect()->route('backend.products.edit', [$product]);
            //->withSuccess('Изменения сохранены.');
    }

    /**
     * Show the form for confirmation delete resource.
     *
     * @param Product $product
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function confirm(Product $product)
    {
        return view('backend.shop.product.confirm', ['product' => $product]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Product $product
     * @param  ImageRepository $imageRepository
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product, ImageRepository $imageRepository)
    {
        $product->delete();

        $imageRepository->init('shop.product_image', $product);
        $imageRepository->deleteFiles();

        return redirect()
            ->route('backend.products.index')
            ->withSuccess('Товар удален.');
    }

    /**
     * @param Request $request
     * @param Product $product
     * @param ImageRepository $imageRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function imgUpload(Request $request, Product $product, ImageRepository $imageRepository)
    {
        if ($request->ajax()) {
            $imageRepository->init('shop.product_image', $product, $request);

            $validator = $imageRepository->validator();
            if ($validator->fails()) {
                return response()->json([
                    'success' => false,
                    'message' => $validator->messages()->first(),
                ], 400);
            }

            $imageRepository->uploadImage();

            return response()->json([
                'success' => true,
                'image'   => $product->response_image, // In model: getResponseImageAttribute()
            ], 200);
        } else {
            return response('Bad Request', 400);
        }
    }

    /**
     * @param Request $request
     * @param Product $product
     * @param ImageRepository $imageRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function imgDelete(Request $request, Product $product, ImageRepository $imageRepository)
    {
        if ($request->ajax()) {
            $imageRepository->init('shop.product_image', $product, $request);
            $imageRepository->deleteImage();

            return response()->json([
                'success' => true,
            ], 200);
        } else {
            return response('Bad Request', 400);
        }
    }


    //------------------------------------------------------------------------------------------------------------------

    /**
     * Add ids of checked categories
     *
     * @param Product $product
     * @param $category_ids array Array of checked categories
     */
    private function syncCategories(Product $product, $category_ids)
    {
        $category_ids[] = $product->category_id;

//        if (in_array($product->category_id, $category_ids)) {
//            $key = array_search($product->category_id, $category_ids);
//            unset($category_ids[$key]);
//        }

        $product->categories()->sync($category_ids);
    }
}
