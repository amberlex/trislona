<?php

Route::group(['middleware' => ['web']], function () {

    // DB
    Route::get('/ms', function() {
        Artisan::call('migrate');
        Artisan::call('db:seed');
        return redirect('/');
    });
    Route::get('/migrate', function() {
        Artisan::call('migrate');
        return redirect('/');
    });
    Route::get('/seed', function() {
        Artisan::call('db:seed');
        return redirect('/');
    });

    // Auth
    Route::auth();

    //------------------------------------------------------------------------------------------------------------------
    // Backend

    Route::get('backend', function () {
        return redirect('backend/dashboard');
    });

    Route::group(['namespace' => 'Backend', 'prefix' => 'backend', 'middleware' => 'backend'], function() {

        // Dashboard
        Route::get('dashboard', ['as' => 'backend.dashboard', 'uses' => 'DashboardController@index']);

        // Settings
        Route::resource('settings', 'SettingsController', ['except' => ['show', 'create', 'story', 'destroy']]);

        // Static Pages
        Route::get('pages/{pages}/confirm',        ['as' => 'backend.pages.confirm', 'uses' => 'PageController@confirm']);
        Route::resource('pages', 'PageController', ['except' => 'show']);

        // Publications
        Route::group(['namespace' => 'Publications'], function() {

            // Rubrics
            Route::get('rubrics/{rubrics}/confirm',        ['as' => 'backend.rubrics.confirm',    'uses' => 'RubricController@confirm']);
            Route::post('rubrics/{rubrics}/img/upload',    ['as' => 'backend.rubrics.img.upload', 'uses' => 'RubricController@imgUpload']);
            Route::post('rubrics/{rubrics}/img/delete',    ['as' => 'backend.rubrics.img.delete', 'uses' => 'RubricController@imgDelete']);
            Route::resource('rubrics', 'RubricController', ['except' => 'show']);

            // Articles
            Route::get('articles/{articles}/confirm',        ['as' => 'backend.articles.confirm',    'uses' => 'ArticleController@confirm']);
            Route::post('articles/{articles}/img/upload',    ['as' => 'backend.articles.img.upload', 'uses' => 'ArticleController@imgUpload']);
            Route::post('articles/{articles}/img/delete',    ['as' => 'backend.articles.img.delete', 'uses' => 'ArticleController@imgDelete']);
            Route::resource('articles', 'ArticleController', ['except' => 'show']);

        });

        // Shop
        Route::group(['namespace' => 'Shop'], function() {

            // Currencies
            Route::get('currencies/{currencies}/confirm',        ['as' => 'backend.currencies.confirm', 'uses' => 'CurrencyController@confirm']);
            Route::resource('currencies', 'CurrencyController',  ['except' => 'show']);

            // Brands
            Route::get('brands/{brands}/confirm',        ['as' => 'backend.brands.confirm',    'uses' => 'BrandController@confirm']);
            Route::post('brands/{brands}/img/upload',    ['as' => 'backend.brands.img.upload', 'uses' => 'BrandController@imgUpload']);
            Route::post('brands/{brands}/img/delete',    ['as' => 'backend.brands.img.delete', 'uses' => 'BrandController@imgDelete']);
            Route::resource('brands', 'BrandController', ['except' => 'show']);

            // Categories
            Route::get('categories/{categories}/confirm',       ['as' => 'backend.categories.confirm',     'uses' => 'CategoryController@confirm']);
            Route::post('categories/{categories}/img/upload',   ['as' => 'backend.categories.img.upload',  'uses' => 'CategoryController@imgUpload']);
            Route::post('categories/{categories}/img/delete',   ['as' => 'backend.categories.img.delete',  'uses' => 'CategoryController@imgDelete']);
            Route::get('categories/{categories}/descendants',   ['as' => 'backend.categories.descendants', 'uses' => 'CategoryController@descendants']);
            Route::resource('categories', 'CategoryController', ['except' => 'show']);

            // Products
            Route::get('products/{products}/confirm',        ['as' => 'backend.products.confirm',    'uses' => 'ProductController@confirm']);
            Route::post('products/{products}/img/upload',    ['as' => 'backend.products.img.upload', 'uses' => 'ProductController@imgUpload']);
            Route::post('products/{products}/img/delete',    ['as' => 'backend.products.img.delete', 'uses' => 'ProductController@imgDelete']);
            Route::resource('products', 'ProductController', ['except' => 'show']);

        });
    });


    //------------------------------------------------------------------------------------------------------------------
    // Frontend

    Route::group(['namespace' => 'Frontend'], function() {

        // Home Page
        Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

        // Contact Page
        Route::get('contact',  ['as' => 'contact.create', 'uses' => 'ContactController@create']);
        Route::post('contact', ['as' => 'contact.store',  'uses' => 'ContactController@store']);

        // Publications
        Route::group(['namespace' => 'Publications'], function() {

            // Rubrics
            Route::get('rubrics',       ['as' => 'publications.rubrics',  'uses' => 'RubricController@index']);
            Route::get('rubric/{slug}', ['as' => 'publications.rubric',   'uses' => 'RubricController@show']);

            // Articles
            Route::get('articles',       ['as' => 'publications.articles', 'uses' => 'ArticleController@index']);
            Route::get('article/{slug}', ['as' => 'publications.article',  'uses' => 'ArticleController@show']);
        });

        // Shop
        Route::group(['namespace' => 'Shop'], function() {

            // Brands
            Route::get('brands',       ['as' => 'shop.brands', 'uses' => 'BrandController@index']);
            Route::get('brand/{slug}', ['as' => 'shop.brand',  'uses' => 'BrandController@show']);

            // Categories
            Route::get('categories',      ['as' => 'shop.categories', 'uses' => 'CategoryController@index']);
            Route::get('category/{slug}', ['as' => 'shop.category',   'uses' => 'CategoryController@show']);

            // Products
            Route::get('product/{slug}', ['as' => 'shop.product', 'uses' => 'ProductController@show']);

        });

        // Static Page
        Route::get('{slug}', ['as' => 'page', 'uses' => 'PageController@index']);

    });
});
