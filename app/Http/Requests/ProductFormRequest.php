<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProductFormRequest extends RequestBackend
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'       => 'required|min:2|max:255',
            'slug'        => 'required|alpha_dash|max:255|unique:shop_products,slug,' . $this->segment(3),
            'price'       => 'required|numeric|min:0.01',
            'category_id' => 'required'
        ];
    }
}
