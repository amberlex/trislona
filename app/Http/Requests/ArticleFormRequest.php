<?php

namespace App\Http\Requests;

class ArticleFormRequest extends RequestBackend
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:2|max:255',
            'slug'  => 'required|alpha_dash|max:255|unique:publications_articles,slug,' . $this->segment(3),
            'published_at' => 'required|date_format:Y-m-d H:i:s'
        ];
    }
}
