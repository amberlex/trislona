<?php

namespace App\Http\Requests;

class RequestBackend extends Request
{
    /**
     * Modify input before validation
     *
     * @return array
     */
    public function all()
    {
        $parameters = parent::all();

        $parameters = trim_array($parameters);
        $parameters = array_map('rm_double_spaces', $parameters);

        if (isset($parameters['published_at'])) {
            $parameters['published_at'] = $parameters['published_at'] . ':00';
        }

        if (isset($parameters['title'])) {

            $parameters['title'] = strip_tags($parameters['title']);

            if (isset($parameters['slug'])) {
                $parameters['slug'] = mb_strtolower($parameters['slug']);
                $parameters['slug'] = strip_tags($parameters['slug']);
                if ($parameters['slug'] == '') {
                    $parameters['slug'] = str_slug($parameters['title']);
                }
            }

            if (isset($parameters['meta_k'])) {
                $parameters['meta_k'] = strip_tags($parameters['meta_k']);
                if ($parameters['meta_k'] == '') {
                    $parameters['meta_k'] = $parameters['title'];
                }
            }

            if (isset($parameters['meta_d'])) {
                $parameters['meta_d'] = strip_tags($parameters['meta_d']);
                if ($parameters['meta_d'] == '') {
                    $parameters['meta_d'] = $parameters['title'];
                }
            }
        }

        return $parameters;
    }
}
