<?php

namespace App\Http\Requests;

class CurrencyFormRequest extends RequestBackend
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'code' => 'required|unique:shop_currencies,code,' . $this->segment(3),
            'rate' => 'required',
            'symbol' => 'required|max:3',
        ];
    }
}
