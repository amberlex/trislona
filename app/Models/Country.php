<?php

namespace App\Models;

use App\Models\Shop\Brand;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'countries';

    /**
     * Get the articles for the rubric.
     */
    public function brands()
    {
        return $this->hasMany(Brand::class, 'country_id');
    }
}
