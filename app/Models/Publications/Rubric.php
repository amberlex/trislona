<?php

namespace App\Models\Publications;

use Illuminate\Database\Eloquent\Model;

class Rubric extends Model
{
    /**
     * @var mixed
     */
    private $config;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'publications_rubrics';

    /**
     * @var array  fields to save
     */
    protected $fillable = [
        'slug',
        'title',
        'description',
        'icon',
        'is_saved',
        'meta_k',
        'meta_d',
    ];

    /**
     * Get the articles for the rubric.
     */
    public function articles()
    {
        return $this->hasMany(Article::class, 'rubric_id');
    }

    /**
     * Rubric constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->config = config('publications');
    }

    /**
     * Get the full path to image.
     */
    public function getAdminImageAttribute()
    {
        return $this->config['rubric_image']['url']
             . $this->config['rubric_image']['thumbnails']['admin']['sub_dir'] . $this->image;
    }

    /**
     * Get the full path to image.
     */
    public function getThImageAttribute()
    {
        return $this->config['rubric_image']['url']
             . $this->config['rubric_image']['thumbnails']['th']['sub_dir'] . $this->image;
    }
}
