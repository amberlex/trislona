<?php

namespace App\Models\Publications;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Article extends Model
{
    /**
     * @var mixed
     */
    private $config;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'publications_articles';

    /**
     * @var array  fields to save
     */
    protected $fillable = [
        'rubric_id',
        'slug',
        'title',
        'preview',
        'content',
        'is_enabled',
        'meta_k',
        'meta_d',
        'published_at',
    ];

    /**
     * The published_at column should be treated as a date.
     *
     * @var array
     */
    protected $dates = ['published_at'];

    /**
     * Get the rubric that owns the article.
     */
    public function rubric()
    {
        return $this->belongsTo(Rubric::class);
    }

    /**
     * Rubric constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->config = config('publications');
    }

    /**
     * To control when the post is to be published.
     *
     * @param $query
     */
    public function scopePublished($query)
    {
        $query->where('published_at', '<=', Carbon::now());
    }

    /**
     * Get published_at value (format method: get{FieldName}Attribute)
     *
     * @param $date
     * @return string
     */
    public function getPublishedAtAttribute($date)
    {
        // Format 'Y-m-d H:i:s' to format 'Y-m-d H:i'
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d H:i');
    }

    /**
     * Get the full path to image (image for backend)
     */
    public function getAdminImageAttribute()
    {
        return $this->config['article_image']['url']
             . $this->config['article_image']['thumbnails']['admin']['sub_dir'] . $this->image;
    }

    /**
     * Get the full path to image.
     */
    public function getThImageAttribute()
    {
        return $this->config['article_image']['url']
             . $this->config['article_image']['thumbnails']['th']['sub_dir'] . $this->image;
    }
}
