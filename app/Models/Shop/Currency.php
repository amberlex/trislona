<?php

namespace App\Models\Shop;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Currency extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shop_currencies';

    /**
     * @var array  fields to save
     */
    protected $fillable = [
        'code',
        'name',
        'symbol',
        'rate',
        'symbol_pos',
        'is_main'   ,
        'is_enabled',
    ];

    /**
     * Get formatted date for display
     *
     * @param string $format
     * @return string
     */
    public function getForDisplay($format = 'Y-m-d H:i')
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->updated_at)->format($format);
    }

    /**
     * Unset old is_main and set new is_main values
     *
     * @param Currency $currency_main
     */
    public function saveIsMain(Currency $currency_main)
    {
        if ($currency_main->id != $this->id) {
            if ($currency_main->is_main AND $this->is_main) {

                $currency_main->is_main = 0;
                $currency_main->save();

                $this->rate = 1.0000;
                $this->is_enabled = 1;
                $this->save();
            }
        }
    }

    /**
     * @return mixed
     */
    public function getForSelect()
    {
        return $this
            ->where('is_enabled', 1)
            ->get()
            ->lists('code', 'id');
    }
}
