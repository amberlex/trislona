<?php

namespace App\Models\Shop;

use App\Models\Country;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    /**
     * @var mixed
     */
    private $config;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shop_brands';

    /**
     * Guard fields from mass-assignment.
     *
     * @var array  fields to save
     */
    protected $guarded = ['id', 'created_at', 'updated_at', 'save_and_exit'];

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }


    /**
     * Rubric constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->config = config('shop');
    }

    /**
     * Get the full path to image.
     */
    public function getAdminImageAttribute()
    {
        return $this->config['brand_image']['url']
        . $this->config['brand_image']['thumbnails']['admin']['sub_dir'] . $this->image;
    }

    /**
     * Get the full path to image.
     */
    public function getThImageAttribute()
    {
        return $this->config['brand_image']['url']
        . $this->config['brand_image']['thumbnails']['th']['sub_dir'] . $this->image;
    }
}
