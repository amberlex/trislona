<?php

namespace App\Models\Shop;

use Illuminate\Database\Eloquent\Model;
use McCool\LaravelAutoPresenter\HasPresenter;
use App\Presenters\ProductPresenter;

class Product extends Model implements HasPresenter
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shop_products';

    /**
     * Guard fields from mass-assignment.
     *
     * @var array  fields to save
     */
    protected $guarded = ['id', 'created_at', 'updated_at', 'category_ids', 'save_and_exit'];

    /**
     * Presenter
     *
     * @return mixed
     */
    public function getPresenterClass()
    {
        return ProductPresenter::class;
    }

    /**
     * Brand of product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class, 'brand_id');
    }

    /**
     * Currency of product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    /**
     * Category of product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    /**
     * Categories of product
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'shop_product_category');
    }

    /**
     * For IDs of additional categories which was selected for products
     *
     * @return mixed
     */
    public function getCategoryIDsAttribute()
    {
        return $this->categories()->lists('id')->all();
    }

    /**
     * Get the full path to image.
     */
    public function getResponseImageAttribute()
    {
        $config = config('shop.product_image');

        return $config['url'] . $config['thumbnails']['admin']['sub_dir'] . $this->image;
    }
}
