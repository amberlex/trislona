<?php

namespace App\Models\Shop;

use Baum\Node;
use McCool\LaravelAutoPresenter\HasPresenter;
use App\Presenters\CategoryPresenter;

class Category extends Node implements HasPresenter
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shop_categories';

    /**
     * @var array  fields to save
     */
    protected $fillable = [
        'slug',
        'title',
        'description',
        'image',
        'path',
        'meta_k',
        'meta_d',
    ];

    public function parent()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Presenter
     *
     * @return mixed
     */
    public function getPresenterClass()
    {
        return CategoryPresenter::class;
    }

    /**
     * Products of category
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class, 'shop_product_category');
    }

    /**
     * Products of category
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function productsOne()
    {
        return $this->hasMany(Product::class, 'category_id');
    }

    /**
     * Save ancestors for path field
     */
    public function updatePath()
    {
        $this->path = $this->getAncestors(['title'])->toJson();
        $this->save();
    }

    /**
     * Update order category
     *
     * @param $order
     * @param $orderPage
     * @return bool
     */
    public function updateOrder($order, $orderPage)
    {
        $orderPage = $this->findOrFail($orderPage);

        if ($order == 'childOf') {

            // Добавлять во второй уровень нельзя
            if ($orderPage->depth == 2) {
                return true;
            }

            // Если есть хоть один потомок, то на 1 уровень нельзя
            if ($orderPage->depth == 1 AND $this->descendants()->get(['id'])->count()) {
                return true;
            }
        }

        if ($order == 'childOf' AND $orderPage->depth == 2) {
            return true;
        }

        if ($order == 'before') {
            $this->moveToLeftOf($orderPage);
        } elseif ($order == 'after') {
            $this->moveToRightOf($orderPage);
        } elseif ($order == 'childOf') {
            $this->makeChildOf($orderPage);
        }

        return false;
    }

    /**
     * Move all products to category id = 1
     */
    public function moveProducts()
    {
        if ($this->isLeaf()) {
            $this->productsOne()->update(['category_id' => 1]);
        } else {
            foreach ($this->getDescendantsAndSelf() as $descendantID) {
                $descendantID->productsOne()->update(['category_id' => 1]);
            }
        }
    }

    /**
     * Get the full path to image.
     */
    public function getResponseImageAttribute()
    {
        $config = config('shop.category_image');

        return $config['url'] . $config['thumbnails']['admin']['sub_dir'] . $this->image;
    }

    public function getRoots()
    {
        return $this
            ->roots()
            ->where('id', '>', 1)
            ->get();
    }

    public function getForSelect()
    {
        return $this
            ->where('id', '>', 1)
            ->orderBy('lft')
            ->get()
            ->lists('path_titles', 'id')
            ->toArray();
    }

    protected function getPathTitlesAttribute()
    {
        $ancestors = json_decode($this->path);

        $path = '';
        foreach($ancestors as $ancestor) {
            $path .= $ancestor->title . ' &rarr; ';
        }

        return $path . $this->title;
    }
}
