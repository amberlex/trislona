<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pages';

    /**
     * @var array  fields to save
     */
    protected $fillable = [
        'slug',
        'title',
        'content',
        'is_enabled',
        'icon',
        'meta_k',
        'meta_d',
    ];
}
