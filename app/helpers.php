<?php

if (! function_exists('rm_double_spaces')) {
    function rm_double_spaces($value) {
        return preg_replace('/\s+/', ' ', $value);
    }
}

if (! function_exists('trim_array')) {
    function trim_array($input) {
        if (!is_array($input)) {
            return trim($input);
        }

        return array_map('trim_array', $input);
    }
}