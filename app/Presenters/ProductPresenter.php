<?php

namespace App\Presenters;

use App\Models\Shop\Product;
use McCool\LaravelAutoPresenter\BasePresenter;

class ProductPresenter extends BasePresenter
{
    /**
     * @var mixed
     */
    private $config;

    public function __construct(Product $resource)
    {
        $this->wrappedObject = $resource;
        $this->config = config('shop.product_image');
    }

    /**
     * Get the full path to image (icon for backend).
     */
    public function admin_image()
    {
        return $this->config['url']
        . $this->config['thumbnails']['admin']['sub_dir'] . $this->image;
    }

    /**
     * Get the full path to image (icon for frontend).
     */
    public function th_image()
    {
        return $this->config['url']
        . $this->config['thumbnails']['th']['sub_dir'] . $this->image;
    }

    /**
     * Get price with symbol of currency
     *
     * @return string
     */
    public function admin_price() {
        return $this->wrappedObject->currency->symbol_pos == 'right'
            ? $this->wrappedObject->price . '&nbsp' . $this->wrappedObject->currency->symbol
            : $this->wrappedObject->currency->symbol . '&nbsp' . $this->wrappedObject->price;
    }

    /**
     * Get price with symbol of currency
     *
     * @return string
     */
    public function priceOnPageUAN() {
        $price = $this->wrappedObject->currency->is_main
            ? $this->wrappedObject->price
            : $this->wrappedObject->price / $this->wrappedObject->currency->rate;

        return number_format($price, 2) . '&nbsp' . 'грн.';
    }
}