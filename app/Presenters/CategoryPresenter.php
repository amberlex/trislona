<?php

namespace App\Presenters;

use App\Models\Shop\Category;
use McCool\LaravelAutoPresenter\BasePresenter;

class CategoryPresenter extends BasePresenter
{
    /**
     * @var mixed
     */
    private $config;

    public function __construct(Category $resource)
    {
        $this->wrappedObject = $resource;
        $this->config = config('shop.category_image');
    }

    /**
     * Get the full path to image (icon for backend).
     */
    public function admin_image()
    {
        return $this->config['url']
        . $this->config['thumbnails']['admin']['sub_dir'] . $this->image;
    }

    /**
     * Get the full path to image (icon for frontend).
     */
    public function th_image()
    {
        return $this->config['url']
        . $this->config['thumbnails']['th']['sub_dir'] . $this->image;
    }

    /**
     * Title with padding
     *
     * @return string
     */
    public function padded_title_all()
    {
        return str_repeat('&mdash; ', $this->wrappedObject->depth) . $this->wrappedObject->title;
    }

    /**
     * Title with padding
     *
     * @return string
     */
    public function padded_title()
    {
        $depth = ($this->wrappedObject->depth > 0)
            ? $this->wrappedObject->depth - 1
            : $this->wrappedObject->depth;

        return str_repeat('&mdash; ', $depth) . $this->wrappedObject->title;
    }

    /**
     * Title with padding as link
     *
     * @param $link
     * @return string
     */
    public function linkToPaddedTitle($link)
    {
        $depth = $this->wrappedObject->depth > 0
            ? $this->wrappedObject->depth - 1
            : $this->wrappedObject->depth;

        $padding = str_repeat('&mdash; ', $depth);

        return $padding . link_to($link, $this->wrappedObject->title);
    }
}