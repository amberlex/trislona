var APP = {

    btnUpload: $('#uploadBtn'),
    msgBox:    $('#msgBox'),
    img:       $('#img'),


    // AJAX Image Upload
    //
    imageUpload: function (objectID, urlUpload) {
        new ss.SimpleUpload({
            button: APP.btnUpload,
            url: urlUpload,
            name: 'image',
            responseType: 'json',
            allowedExtensions: ['jpeg', 'jpg', 'png', 'gif'],
            onComplete: function(filename, response) {
                if (!response) {
                    alert('Upload failed');
                    return false;
                }
                if (response.success === true) {
                    APP.img.attr('src', response.image).show();
                    $('#deleteBtn' + objectID).show()
                } else {
                    if (response.message) {
                        msgBox.html(response.message);
                    } else {
                        msgBox.html('Не удалось загрузить файл.');
                    }
                }
            },
            onError: function() {
                msgBox.html('Ошибка при загрузке.');
            }
        });
    },

    // AJAX Image Delete
    //
    imageDelete: function (objectID, urlDelete) {
        if ( ! confirm('Удалить изображение?')) {
            return false;
        }
        $.ajax({
            url: urlDelete,
            type: 'post',
            dataType: 'json',
            success: function (response) {
                if (response.success) {
                    APP.img.attr('src', '#').hide();
                    $('#deleteBtn' + objectID).hide();
                } else {
                    msgBox.html('Ошибка при удалении.');
                }
            },
            error: function () {
                alert('Upload failed');
            }
        });
    }
};

$(function () {
    $('#datetimepicker2').datetimepicker({ });
});
