var APP = {

    /**
     * Text messages
     */
    msg: {
        err: {
            transmission: 'Transmission data error!'
        }
    },
};