<div class="row">

    <div class="col-md-3">
        <div class="panel panel-default">
            <div class="panel-body">

                <p class="lead"><a href="{{ route('home') }}">Интернет-магазин - Три Слона</a></p>
                <p>Товары для дома - у нас есть всё!</p>

            </div>
        </div>
    </div>

    <div class="col-md-2">
        <div class="panel panel-default">
            <div class="panel-body">

                <div class="nowrap"><i class="fa fa-phone fa-fw"></i>(098) 704-79-65</div><br>
                <div class="nowrap"><i class="fa fa-phone fa-fw"></i>(093) 909-65-35</div><br>
                <div class="nowrap"><i class="fa fa-phone fa-fw"></i>(095) 681-54-75</div>

            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="panel panel-default">
            <div class="panel-body">
                @widget('navPages', ['view' => 'nav_header'])
            </div>
        </div>
    </div>

    <div class="col-md-2">
        <div class="panel panel-default">
            <div class="panel-body">

                @if (Auth::guest())

                    <div class="nowrap">
                        <i class="fa fa-sign-in fa-fw"></i>
                        <a href="{{ url('/login') }}">Вход</a>
                    </div>
                    <br>

                    <div class="nowrap">
                        <i class="fa fa-user-plus fa-fw"></i>
                        <a href="{{ url('/register') }}">Регистрация</a>
                    </div>
                    <br>

                    <div class="nowrap">
                        <i class="fa fa-key fa-fw"></i>
                        <a href="{{ url('/password/reset') }}">Забыли пароль?</a>
                    </div>

                @else

                    <div class="nowrap">
                        <i class="fa fa-user fa-fw"></i>
                        Привет, {{ Auth::user()->name }}!
                    </div>
                    <br>

                    <div class="nowrap">
                        <i class="fa fa-edit fa-fw"></i>
                        <a href="#">Мой профиль</a>
                    </div>
                    <br>

                    <div class="nowrap">
                        <i class="fa fa-sign-out fa-fw"></i>
                        <a href="{{ url('/logout') }}">Выход</a>
                    </div>

                    @if(Auth::user()->is_admin)
                        <div class="nowrap">
                            <i class="fa fa-cogs fa-fw"></i>
                            <a href="{{ url('/backend') }}">Админка</a>
                        </div>
                    @endif

                @endif

            </div>
        </div>
    </div>

    <div class="col-md-2">
        <div class="panel panel-default">
            <div class="panel-body">

                <p class="lead">
                    <i class="fa fa-shopping-cart fa-fw"></i>
                    <a href="#">Корзина</a>
                </p>
                <p>Товаров нет</p>

            </div>
        </div>
    </div>

</div>
