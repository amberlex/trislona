@extends('frontend.layouts.pages')

@section('title', 'Рубрики')

@section('content')

    @widget('Breadcrumbs', ['breadcrumbs' => $breadcrumbs])

    <div class="page-header">
        <h1>Рубрики</h1>
    </div>

    @forelse ($rubrics as $rubric)
        <div class="row">
            <div class="col-sm-3">
                @if($rubric->image)
                    <a href="{{ route('publications.rubric', $rubric->slug) }}" class="thumbnail">
                        <img src="{{ $rubric->th_image }}" alt="{{ $rubric->title }}">
                    </a>
                @endif
            </div>
            <div class="col-sm-9">
                <h2>
                    {!! Html::linkRoute('publications.rubric', $rubric->title, $rubric->slug) !!}
                </h2>
                {!! $rubric->description !!}
            </div>
        </div>
    @empty
        <p class="lead">Рубрик нет</p>
    @endforelse

@stop