@extends('frontend.layouts.pages')

@section('title', $rubric->title)

@section('content')

    @widget('Breadcrumbs', ['breadcrumbs' => $breadcrumbs])

    <div class="page-header">
        <h1>{{ $rubric->title }}</h1>
    </div>
    {!! $rubric->description !!}
    <hr>

    @forelse ($articles as $article)
        <div class="row">
            <div class="col-sm-3">
                @if($article->image)
                    <a href="{{ route('publications.article', $article->slug) }}" class="thumbnail">
                        <img src="{{ $article->th_image }}" alt="{{ $article->title }}">
                    </a>
                @endif
            </div>
            <div class="col-sm-9">
                <h2>
                    {!! Html::linkRoute('publications.article', $article->title, $article->slug) !!}
                </h2>
                {!! $article->preview !!}
                <p>
                    <i class="fa fa-calendar fa-fw"></i>
                    <small>{{ $article->published_at }}</small>
                </p>
            </div>
        </div>
    @empty
        <p class="lead">Публикаций нет</p>
    @endforelse

@stop