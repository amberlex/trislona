@extends('frontend.layouts.pages')

@section('title', $article->title)

@section('content')

    @widget('Breadcrumbs', ['breadcrumbs' => $breadcrumbs])

    <div class="page-header">
        <h1>{{ $article->title }}</h1>
    </div>

    <p>
        <i class="fa fa-folder-o fa-fw"></i>
        {!! Html::linkRoute('publications.rubric', $article->rubric->title, $article->rubric->slug) !!}
        <i class="fa fa-calendar fa-fw"></i>
        <small>{{ $article->published_at }}</small>
    </p>
    <hr>

    @if($article->image)
        <img class="img-thumbnail img-responsive img-page" src="{{ $article->th_image }}" alt="{{ $article->title }}" width="350">
    @endif
    {!! $article->preview !!}
    {!! $article->content !!}

@stop