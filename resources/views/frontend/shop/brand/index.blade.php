@extends('frontend.layouts.pages')

@section('title', 'Бренды')

@section('content')

    @widget('Breadcrumbs', ['breadcrumbs' => $breadcrumbs])

    <div class="page-header">
        <h1>Бренды</h1>
    </div>

    @if($brands->count())
        @foreach ($brands as $brand)
            <div class="row">

                <div class="col-sm-2">
                    @if($brand->image)
                        <a href="{{ route('shop.brand', ['slug' => $brand->slug]) }}" class="thumbnail">
                            <img src="{{ $brand->th_image }}" alt="{{ $brand->title }}">
                        </a>
                    @endif
                </div>
                <div class="col-sm-10">
                    <h2>
                        {!! Html::linkRoute('shop.brand', $brand->title, ['slug' => $brand->slug]) !!}
                    </h2>
                    {!! $brand->preview !!}
                </div>

            </div>
        @endforeach
    @else
        <p class="lead">Список брендов пуст.</p>
    @endif

@stop