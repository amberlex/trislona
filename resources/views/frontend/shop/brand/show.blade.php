@extends('frontend.layouts.pages')

@section('title', $brand->title)

@section('content')

    @widget('Breadcrumbs', ['breadcrumbs' => $breadcrumbs])

    <div class="page-header">
        <h1>{{ $brand->title }}</h1>
    </div>

    @if($brand->image)
        <img class="img-thumbnail img-responsive img-page" src="{{$brand->th_image}}" alt="{{ $brand->title }}" width="200">
    @endif
    {!! $brand->preview !!}
    {!! $brand->content !!}
    <hr>

@stop