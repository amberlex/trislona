@extends('frontend.layouts.shop')

@section('title', $product->title)

@section('content')

    @widget('Breadcrumbs', ['breadcrumbs' => $breadcrumbs])

    <div class="row">
        <div class="col-sm-4">
            @if($product->image)
                <img class="img-thumbnail img-responsive img-page" src="{{$product->th_image}}" alt="{{ $product->title }}">
            @endif
        </div>
        <div class="col-sm-8">

            <div class="page-header">
                <h1>{{ $product->title }}</h1>
            </div>

            {!! $product->preview !!}

            @if($product->brand->id > 1)
                <p>
                    <strong>Бренд:</strong>
                    {{ $product->brand->title }}
                    @if($product->brand->country->id > 1)
                        ({{ $product->brand->country->title }})
                    @endif
                </p>
            @endif

            <p>
                <strong>Цена:</strong>
                <span class="label label-primary">{!! $product->priceOnPageUAN !!}</span>
            </p>
            <a href="#" class="btn-add-to-cart btn btn-success">
                <i class="fa fa-plus fa-fw"></i> В корзину
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            {!! $product->content !!}
        </div>
    </div>
@stop