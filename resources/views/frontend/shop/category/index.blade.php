@extends('frontend.layouts.pages')

@section('title', 'Категории товаров')

@section('content')

    @widget('Breadcrumbs', ['breadcrumbs' => $breadcrumbs])

    <div class="page-header">
        <h1>Категории товаров</h1>
    </div>

    @forelse ($categories as $category)
        <div class="row">
            <div class="col-sm-3">
                @if($category->image)
                    <a href="{{ route('shop.category', ['slug' => $category->slug]) }}" class="thumbnail">
                        <img src="{{ $category->th_image }}" alt="{{ $category->title }}">
                    </a>
                @endif
            </div>
            <div class="col-sm-9">
                <h2>
                    <a href="{{ route('shop.category', ['slug' => $category->slug]) }}">
                        {{ $category->title }}
                    </a>
                </h2>
                {!! $category->description !!}
            </div>
        </div>
    @empty
        <p class="lead">Категорий нет.</p>
    @endforelse

@stop