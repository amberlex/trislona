@extends('frontend.layouts.shop')

@section('title', 'Категории товаров')

@section('content')

    @widget('Breadcrumbs', ['breadcrumbs' => $breadcrumbs])

    <div class="page-header">
        <h1>{{ $category->title }}</h1>
    </div>

    @if($descendants->count())
        @foreach($descendants as $descendant)
            <div class="row">
                <div class="col-sm-3">
                    @if($descendant->image)
                        <a href="{{ route('shop.category', ['slug' => $descendant->slug]) }}" class="thumbnail">
                            <img src="{{ $descendant->th_image }}" alt="{{ $descendant->title }}">
                        </a>
                    @endif
                </div>
                <div class="col-sm-9">
                    <h2>
                        <a href="{{ route('shop.category', ['slug' => $descendant->slug]) }}">
                            {{ $descendant->title }}
                        </a>
                    </h2>

                    {!! $descendant->description !!}

                    @if($descendant->children->count())
                        <p>
                            @foreach($descendant->children as $child)
                                <a href="{{ route('shop.category', ['slug' => $child->slug]) }}" class="btn btn-default btn-sm">
                                    {{ $child->title }}
                                </a>
                            @endforeach
                        </p>
                    @endif

                </div>
            </div>
        @endforeach
        <hr>
    @endif

    @foreach ($products as $product)
        <div class="row">
            <div class="col-sm-3">
                @if($product->image)
                    <a href="{{ route('shop.product', ['slug' => $product->slug]) }}" class="thumbnail">
                        <img src="{{ $product->th_image }}" alt="{{ $product->title }}">
                    </a>
                @endif
            </div>
            <div class="col-sm-9">
                <h2><a href="{{ route('shop.product', $product->slug) }}">{{ $product->title }}</a></h2>
                {!! $product->preview !!}
                <p><strong>Цена:</strong> <span class="label label-primary">{!! $product->priceOnPageUAN !!}</span></p>
                <a href="#" class="btn-add-to-cart btn btn-success btn-xs">
                    <i class="fa fa-plus fa-fw"></i> В корзину
                </a>
                <hr>
            </div>
        </div>
    @endforeach

@stop