@extends('frontend.layouts.pages')

@section('title', 'Контакты')

@section('content')

    @widget('Breadcrumbs', ['breadcrumbs' => $breadcrumbs])

    <div class="page-header">
        <h1>Контакты</h1>
    </div>

    <div class="row">
        <div class="col-lg-5">
            {!! Form::open(['route' => 'contact.store']) !!}

            <div class="form-group">
                {!! Form::label('name', 'Имя') !!}
                {!! Form::text('name', null, [
                    'required',
                    'class' => 'form-control',
                    'placeholder' => 'Ваше имя'
                ]) !!}
            </div>

            <div class="form-group">
                {!! Form::label('email', 'Email адрес') !!}
                {!! Form::email('email', null, [
                    'required',
                    'class' => 'form-control',
                    'placeholder' => 'example@gmail.com'
                ]) !!}
            </div>

            <div class="form-group">
                {!! Form::label('message', 'Сообщение') !!}
                {!! Form::textarea('message', null, [
                    'required',
                    'class' => 'form-control',
                    'placeholder' => 'Ваше сообщение',
                    'rows' => 8
                ]) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Отправить сообщение', ['class' => 'btn btn-primary']) !!}
            </div>

            {!! Form::close() !!}
        </div>
        <div class="col-lg-7">
            <p>
                Мы  находимся по адресу:<br>
                г. Киев, ул. Минина, 12  оф.4
            </p>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2540.2702039260435!2d30.624873815625968!3d50.454692894940464!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40d4c555aa733bdf%3A0x79a5f8fe599823f2!2z0LLRg9C7LiDQnNGW0L3RltC90LAsIDEyLCA0LCDQmtC40ZfQsg!5e0!3m2!1sru!2sua!4v1454420139324" width="600" height="296" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </div>

@stop