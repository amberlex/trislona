<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Три Слона - @yield('title')</title>
    <link href="/public/assets/css/all.css" rel="stylesheet">
    @yield('styles')
</head>
<body>

<div id="wrap">
    <div class="container">
        @include('frontend.header')
        @widget('NavHeaderCategories')

        <div class="row">
            <div class="col-lg-3">
                @widget('navPages')
            </div>
            <div class="col-lg-9">
                @yield('breadcrumbs')
                @include('frontend.messages')
                @yield('content')
            </div>
        </div>

    </div>
</div>
@include('frontend.footer')

<script src="/public/assets/js/all.js"></script>
@yield('scripts')
</body>
</html>