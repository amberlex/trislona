@extends('frontend.layouts.pages')

@section('title', $page->title)

@section('content')

    @widget('Breadcrumbs', ['breadcrumbs' => $breadcrumbs])

    <div class="page-header">
        <h1>{{ $page->title }}</h1>
    </div>
    <p class="lead">{{ $page->content }}</p>

@stop
