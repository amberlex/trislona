@if (Session::has('success'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p><strong><i class="fa fa-check-circle fa-fw"></i> Success.</strong></p>
        <p>{{ Session::get('success') }}</p>
    </div>
@endif

@if (Session::has('status'))
    <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <p><strong><i class="fa fa-check-circle fa-fw"></i> Status.</strong></p>
        <p>{{ Session::get('status') }}</p>
    </div>
@endif

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <p><strong><i class="fa fa-exclamation-circle fa-fw"></i> Errors!</strong></p>
        @foreach ($errors->all() as $error)
            <p><i class="fa fa-caret-right fa-fw"></i> {{ $error }}</p>
        @endforeach
    </div>
@endif