@extends('backend.layouts.default')

@section('title', 'Добавить страницу')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header">
                Статические страницы<small><i class="fa fa-angle-right fa-fw"></i>Добавить страницу</small>
            </h1>
        </div>
    </div>

    {!! Form::open([
        'route'  => 'backend.pages.store',
        'class'  => 'form-horizontal'
    ]) !!}

        @include ('backend.page._form')
        @include ('backend.form_buttons')

    {!! Form::close() !!}

@stop
