@extends('backend.layouts.default')

@section('title', 'Управление страницами')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header">
                Статические страницы<small><i class="fa fa-angle-right fa-fw"></i>Управление страницами</small>
            </h1>
            <div class="btn-group">
                <a href="{{ route('backend.pages.create') }}" class="btn btn-success btn-sm">
                    <i class="fa fa-plus-circle fa-fw"></i> Добавить страницу
                </a>
            </div><hr>
        </div>
    </div>

    @if($pages->count())
        <table class="table table-striped table-hover">

            <thead>
            <tr>
                <th style="width: 75%; white-space: nowrap;">Заголовок страницы</th>
                <th>Статус</th>
                <th>Действия</th>
            </tr>
            </thead>

            <tbody>
            @foreach($pages as $page)
                <tr>
                    <td>
                        <a href="{{ route('backend.pages.edit', $page->id ) }}">
                            {{ $page->title }}
                        </a>
                    </td>
                    <td>
                        @if($page->is_enabled)
                            <i class="fa fa-check-circle fa-lg fa-fw text-success"></i>
                        @else
                            <i class="fa fa-check-circle fa-lg fa-fw text-danger"></i>
                        @endif
                    </td>
                    <td class="icon_actions">
                        <a href="{{ route('backend.pages.edit', $page->id ) }}" >
                            <i class="fa fa-pencil fa-lg fa-fw"></i></a>

                        <a href="{{ route('page', $page->slug) }}" target="_blank">
                            <i class="fa fa-eye fa-lg fa-fw"></i></a>

                        <a href="{{ route('backend.pages.confirm', $page->id) }}">
                            <i class="fa fa-remove fa-lg fa-fw text-danger"></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>

        </table>
    @else
        <p>Страниц нет!</p>
    @endif

@stop
