@extends('backend.layouts.default')

@section('title', 'Удалить ' . $page->title)

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header">
                Статические страницы<small><i class="fa fa-angle-right fa-fw"></i>Удалить страницу</small>
            </h1>
        </div>
    </div>

    {!! Form::open([
        'method' => 'DELETE',
        'route'  => ['backend.pages.destroy', $page->id]
    ]) !!}

    <div class="alert alert-danger">
        <p><strong>Внимание!</strong> Вы действительно хотите удалить эту страницу?</p>
        <p><i class="fa fa-file-o fa-fw"></i> {{ $page->title }}</p>
    </div>

    {!! Form::submit('Да, удалить эту страницу!', ['class' => 'btn btn-danger']) !!}

    <a href="{{ route('backend.pages.index') }}" class="btn btn-success">
        <strong>Нет, вернуться назад!</strong>
    </a>

    {!! Form::close() !!}

@stop
