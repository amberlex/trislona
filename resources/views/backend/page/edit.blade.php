@extends('backend.layouts.default')

@section('title', 'Редактировать страницу')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header">
                Статические страницы<small><i class="fa fa-angle-right fa-fw"></i>Редактировать страницу</small>
            </h1>
        </div>
    </div>

    {!! Form::model($page, [
        'method' => 'PATCH',
        'route'  => ['backend.pages.update', $page->id],
        'class'  => 'form-horizontal'
    ]) !!}

        @include ('backend.page._form')
        @include ('backend.form_buttons')

    {!! Form::close() !!}

@stop
