<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active">
        <a href="#page" aria-controls="page" role="tab" data-toggle="tab">Страница</a>
    </li>
    <li role="presentation">
        <a href="#seo" aria-controls="seo" role="tab" data-toggle="tab">SEO</a>
    </li>
</ul>
<br>

<div class="tab-content">
    <div role="tabpanel" class="tab-pane fade in active" id="page">

        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
            {!! Form::label('title', 'Заголовок страницы', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('title', null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('content', 'Текст страницы', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('icon', 'Название иконки', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-sm-10">
                {!! Form::text('icon', null, ['class' => 'form-control', 'placeholder' => 'fa-info']) !!}
                <span class="help-block">
                    <a href="http://fortawesome.github.io/Font-Awesome/icons" target="_blank">
                        Набор допустимых иконок <small><i class="fa fa-external-link fa-fw"></i></small>
                    </a>
                </span>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-10 col-md-offset-2">
                <div class="checkbox">
                    <label>
                        {!! Form::hidden('is_enabled', 0) !!}
                        {!! Form::checkbox('is_enabled') !!} Публиковать?
                    </label>
                </div>
            </div>
        </div>

    </div>
    <div role="tabpanel" class="tab-pane fade" id="seo">

        @include('backend.form_seo')

    </div>
</div>

@section('scripts')
    <script src="/vendor/midium/laravel-ckeditor/ckeditor.js"></script>
    <script>
        $(function () {
            CKEDITOR.replace('content', { customConfig: '/public/assets/backend/js/ck_content.js' });
        });
    </script>
@stop
