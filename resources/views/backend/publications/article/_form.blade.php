<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active">
        <a href="#page" aria-controls="page" role="tab" data-toggle="tab">Статья</a>
    </li>
    <li role="presentation">
        <a href="#seo" aria-controls="seo" role="tab" data-toggle="tab">SEO</a>
    </li>
</ul>
<br>

<div class="tab-content">
    <div role="tabpanel" class="tab-pane fade in active" id="page">

        <div class="form-group">
            {!! Form::label('rubric_id', 'Рубрика', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::select('rubric_id', $rubrics, null, ['id' => 'rubric_id', 'class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
            {!! Form::label('title', 'Заголовок статьи', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('title', null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('preview', 'Анонс', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::textarea('preview', null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('content', 'Текст статьи', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group{{ $errors->has('published_at') ? ' has-error' : '' }}">
            {!! Form::label('published_at', 'Дата публикации', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('published_at', isset($article) ? null : date('Y-m-d H:i', time()), ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('published_at', 'Статус публикации', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                <div class="checkbox">
                    <label>
                        {!! Form::hidden('is_enabled', 0) !!}
                        {!! Form::checkbox('is_enabled') !!} Опубликовано
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('image', 'Изображение', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                @if (isset($article))
                    @include('backend.form_image', ['object' => $article])
                @else
                    <div class="alert alert-warning" role="alert">
                        Для загрузки изображения сохраните статью.
                    </div>
                @endif
            </div>
        </div>

    </div>
    <div role="tabpanel" class="tab-pane fade" id="seo">

        @include('backend.form_seo')

    </div>
</div>

@section('scripts')
    <script src="/vendor/midium/laravel-ckeditor/ckeditor.js"></script>
    <script>
        $(function () {
            CKEDITOR.replace('preview', { customConfig: '/public/assets/backend/js/ck_preview.js' });
            CKEDITOR.replace('content', { customConfig: '/public/assets/backend/js/ck_content.js' });

            $('#published_at').datetimepicker({
                locale: 'ru',
                format: 'YYYY-MM-DD HH:mm'
            });

            <?php if (isset($article)): ?>
                var objectID = '{{ $article->id }}';
                APP.imageUpload(objectID, '{{ route('backend.articles.img.upload', $article->id) }}');
                $('#deleteBtn' + objectID).click(function(e) {
                    e.preventDefault();
                    APP.imageDelete(objectID, '{{ route('backend.articles.img.delete', $article->id) }}');
                });
            <?php endif ?>
        });
    </script>
@stop
