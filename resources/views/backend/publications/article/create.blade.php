@extends('backend.layouts.default')

@section('title', 'Добавить статью')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header">
                Публикации<small><i class="fa fa-angle-right fa-fw"></i>Добавить статью</small>
            </h1>
        </div>
    </div>

    {!! Form::open([
        'route'  => 'backend.articles.store',
        'class'  => 'form-horizontal'
    ]) !!}

    @include ('backend.publications.article._form')
    @include ('backend.form_buttons')

    {!! Form::close() !!}

@stop
