@extends('backend.layouts.default')

@section('title', 'Редактировать статью')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header">
                Публикации<small><i class="fa fa-angle-right fa-fw"></i>Редактировать статью</small>
            </h1>
        </div>
    </div>

    {!! Form::model($article, [
        'method' => 'PATCH',
        'route'  => ['backend.articles.update', $article->id],
        'class'  => 'form-horizontal'
    ]) !!}

    @include ('backend.publications.article._form')
    @include ('backend.form_buttons')

    {!! Form::close() !!}

@stop
