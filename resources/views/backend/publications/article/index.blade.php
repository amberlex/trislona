@extends('backend.layouts.default')

@section('title', 'Управление статьями')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header">
                Публикации<small><i class="fa fa-angle-right fa-fw"></i>Управление статьями</small>
            </h1>
            <div class="btn-group">
                <a href="{{ route('backend.articles.create') }}" class="btn btn-success btn-sm">
                    <i class="fa fa-plus-circle fa-fw"></i> Добавить статью
                </a>
            </div><hr>
        </div>
    </div>

    @if($articles->count())
        <table class="table table-striped table-hover">

            <thead>
            <tr>
                <th>Изображение</th>
                <th style="width: 75%; white-space: nowrap;">Заголовок статьи</th>
                <th>Статус</th>
                <th>Действия</th>
            </tr>
            </thead>

            <tbody>
            @foreach($articles as $article)
                <tr>
                    <td>
                        @if($article->image)
                            <img src="{{ $article->admin_image }}" alt="" width="52">
                        @endif
                    </td>
                    <td>
                        <a href="{{ route('backend.articles.edit', $article->id ) }}">
                            {{ $article->title }}
                        </a><br>
                        <small>Рубрика: {{ $article->rubric->title }}</small>
                    </td>
                    <td>
                        @if($article->is_enabled)
                            <i class="fa fa-check-circle fa-lg fa-fw text-success"></i>
                        @else
                            <i class="fa fa-check-circle fa-lg fa-fw text-danger"></i>
                        @endif
                    </td>
                    <td class="icon_actions">
                        <a href="{{ route('backend.articles.edit', $article->id ) }}" >
                            <i class="fa fa-pencil fa-lg fa-fw"></i></a>

                        <a href="{{ route('publications.article', $article->slug) }}" target="_blank">
                            <i class="fa fa-eye fa-lg fa-fw"></i></a>

                        <a href="{{ route('backend.articles.confirm', $article->id) }}">
                            <i class="fa fa-remove fa-lg fa-fw text-danger"></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>

        </table>

        {!! $articles->render() !!}

    @else
        <p>Статей нет!</p>
    @endif

@stop
