@extends('backend.layouts.default')

@section('title', 'Удалить статью' . $article->title)

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header">
                Публикации<small><i class="fa fa-angle-right fa-fw"></i>Удалить статью</small>
            </h1>
        </div>
    </div>

    {!! Form::open([
        'method' => 'DELETE',
        'route'  => ['backend.articles.destroy', $article->id]
    ]) !!}

    <div class="alert alert-danger">
        <p><strong>Внимание!</strong> Вы действительно хотите удалить эту статью?</p>
        <p><i class="fa fa-file-o fa-fw"></i> {{ $article->title }}</p>
    </div>

    {!! Form::submit('Да, удалить эту статью!', ['class' => 'btn btn-danger']) !!}

    <a href="{{ route('backend.articles.index') }}" class="btn btn-success">
        <strong>Нет, вернуться назад!</strong>
    </a>

    {!! Form::close() !!}

@stop
