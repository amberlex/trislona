@extends('backend.layouts.default')

@section('title', 'Добавить рубрику')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header">
                Публикации<small><i class="fa fa-angle-right fa-fw"></i>Добавить рубрику</small>
            </h1>
        </div>
    </div>

    {!! Form::open([
        'route'  => 'backend.rubrics.store',
        'class'  => 'form-horizontal'
    ]) !!}

    @include ('backend.publications.rubric._form')
    @include ('backend.form_buttons')

    {!! Form::close() !!}

@stop
