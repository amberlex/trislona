@extends('backend.layouts.default')

@section('title', 'Управление Рубриками')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header">
                Публикации<small><i class="fa fa-angle-right fa-fw"></i>Управление Рубриками</small>
            </h1>
            <div class="btn-group">
                <a href="{{ route('backend.rubrics.create') }}" class="btn btn-success btn-sm">
                    <i class="fa fa-plus-circle fa-fw"></i> Добавить рубрику
                </a>
            </div><hr>
        </div>
    </div>

    @if($rubrics->count())
        <table class="table table-striped table-hover">

            <thead>
            <tr>
                <th>Изображение</th>
                <th style="white-space: nowrap;">Заголовок</th>
                <th style="width: 70%">Описание</th>
                <th>Действия</th>
            </tr>
            </thead>

            <tbody>
            @foreach($rubrics as $rubric)
                <tr>
                    <td>
                        @if($rubric->image)
                            <img src="{{ $rubric->admin_image }}" alt="" width="52">
                        @endif
                    </td>
                    <td>
                        <a href="{{ route('backend.rubrics.edit', $rubric->id ) }}">
                            {{ $rubric->title }}
                        </a>
                    </td>
                    <td>
                        {!! $rubric->description !!}
                    </td>
                    <td class="icon_actions">
                        <a href="{{ route('backend.rubrics.edit', $rubric->id ) }}" >
                            <i class="fa fa-pencil fa-lg fa-fw"></i></a>

                        <a href="{{ route('publications.rubric', $rubric->slug) }}" target="_blank">
                            <i class="fa fa-eye fa-lg fa-fw"></i></a>

                        <a href="{{ route('backend.rubrics.confirm', $rubric->id) }}">
                            <i class="fa fa-remove fa-lg fa-fw text-danger"></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>

        </table>
    @else
        <p>Рубрик нет.</p>
    @endif

@stop
