@extends('backend.layouts.default')

@section('title', 'Редактировать рубрику')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header">
                Публикации
                <small>
                    <i class="fa fa-angle-right fa-fw"></i>
                    @if($rubric->title)
                        Редактировать рубрику
                    @else
                        Добавить рубрику
                    @endif
                </small>
            </h1>
        </div>
    </div>

    {!! Form::model($rubric, [
        'method' => 'PATCH',
        'route'  => ['backend.rubrics.update', $rubric->id],
        'class'  => 'form-horizontal'
    ]) !!}

    @include ('backend.publications.rubric._form')
    @include ('backend.form_buttons')

    {!! Form::close() !!}

@stop
