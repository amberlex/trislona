@extends('backend.layouts.default')

@section('title', 'Удалить рубрику' . $rubric->title)

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header">
                Публикации<small><i class="fa fa-angle-right fa-fw"></i>Удалить рубрику</small>
            </h1>
        </div>
    </div>

    {!! Form::open([
        'method' => 'DELETE',
        'route'  => ['backend.rubrics.destroy', $rubric->id]
    ]) !!}

    <div class="alert alert-danger">
        <p><strong>Внимание!</strong> Вы действительно хотите удалить эту рубрику?</p>
        <p><i class="fa fa-folder-open-o fa-fw"></i> {{ $rubric->title }}</p>
    </div>

    {!! Form::submit('Да, удалить эту рубрику!', ['class' => 'btn btn-danger']) !!}

    <a href="{{ route('backend.rubrics.index') }}" class="btn btn-success">
        <strong>Нет, вернуться назад!</strong>
    </a>

    {!! Form::close() !!}

@stop
