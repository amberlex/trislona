<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Магазин TriSlona - @yield('title')</title>
    <link href="/public/assets/backend/css/all.css" rel="stylesheet">
    @yield('styles')
</head>

<body>

<div id="wrapper">

    <nav class="navbar navbar-inverse navbar-fixed-top">
        @include('backend.navbar')
        @include('backend.sidebar')
    </nav>

    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @include('backend.messages')
                    @yield('content')
                </div>
            </div>
        </div>
    </div>

</div>

<script src="/public/assets/backend/js/all.js"></script>
@yield('scripts')

</body>
</html>
