<div id="msgBox"></div>
<div id="imgBox">
    @if($object->image)
        <img id="img" src="{{ $object->admin_image }}" alt="" class="img-thumbnail img-single">
    @else
        <img id="img" src="#" alt="" class="img-thumbnail img-single" style="display: none">
    @endif
</div>
<div id="btnsBox">
    @if($object->image)
        <button id="deleteBtn{{ $object->id }}" class="btn btn-danger">
            <i class="fa fa-remove"></i>
        </button>
    @else
        <button id="deleteBtn{{ $object->id }}" class="btn btn-danger" style="display: none">
            <i class="fa fa-remove"></i>
        </button>
    @endif
    <button id="uploadBtn" class="btn btn-default">Выбрать файл...</button>
</div>
