<div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="{{ route('backend.dashboard') }}">Магазин TriSlona</a>
</div>

<ul class="nav navbar-right top-nav">

    <li class="dropdown<?php if ($module == 'settings') echo ' active' ?>">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-cogs fa-fw"></i>
            Настройки
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li>
                <a href="{{ route('backend.settings.index') }}">
                    <i class="fa fa-cog fa-fw"></i> Глобальные настройки
                </a>
            </li>
            <li>
                <a href="{{ route('backend.currencies.index') }}">
                    <i class="fa fa-money fa-fw"></i> Валюты
                </a>
            </li>
        </ul>
    </li>

    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-user"></i>
            Привет, {{ Auth::user()->name }}
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li>
                <a href="#"><i class="fa fa-user fa-fw"></i> Мой профиль</a>
            </li>
            <li>
                <a href="{{ url('/logout') }}"><i class="fa fa-sign-out fa-fw"></i> Выход</a>
            </li>
            <li class="divider"></li>
            <li>
                <a href="{{ route('home') }}"><i class="fa fa-home fa-fw"></i> На сайт</a>
            </li>
        </ul>
    </li>

</ul>
