<div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
    {!! Form::label('slug', 'URL', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::text('slug', null, ['class' => 'form-control']) !!}
        <span class="help-block">
            Вариант названия, подходящий для URL. Может содержать только латинские буквы в нижнем регистре, цифры и дефисы.<br>
            Если не заполнять — будет сгенерирован по названию.
        </span>
    </div>
</div>

<div class="form-group">
    {!! Form::label('meta_d', 'Meta Description', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::textarea('meta_d', null, ['class' => 'form-control', 'rows' => 4]) !!}
        <span class="help-block">
            Это описание будет использовано в <b>meta description</b>.<br>
            Если не заполнять — будет совпадать с заголовком.
        </span>
    </div>
</div>

<div class="form-group">
    {!! Form::label('meta_k', 'Meta Keywords', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::textarea('meta_k', null, ['class' => 'form-control', 'rows' => 4]) !!}
        <span class="help-block">
            Ключевые слова через запятую, которые будут использованы в <b>meta keywords</b>.<br>
            Если не заполнять — будет совпадать с заголовком.
        </span>
    </div>
</div>
