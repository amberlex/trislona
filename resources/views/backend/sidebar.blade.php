<div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav side-nav">

        {{-- Панель управления --}}

        <li<?php if ($module == 'dashboard') echo ' class="active"' ?>>
            <a href="{{ route('backend.dashboard') }}"><i class="fa fa-fw fa-dashboard"></i> Панель управления</a>
        </li>


        {{-- Каталог товаров --}}

        <li<?php if ($module == 'shop') echo ' class="active"' ?>>
            <a href="#" class="dropdown-toggle collapsed" data-toggle="collapse" data-target="#shop">
                <i class="fa fa-fw fa-folder-o"></i> Каталог товаров
            </a>
            <ul id="shop" class="collapse<?php if ($module == 'shop') echo ' in' ?>">
                <li<?php if ($current_route == 'backend.products.index') echo ' class="active"' ?>>
                    <a href="{{ route('backend.products.index') }}">
                        <i class="fa fa-fw fa-list-ul"></i> Управление товарами
                    </a>
                </li>
                <li<?php if ($current_route == 'backend.products.create') echo ' class="active"' ?>>
                    <a href="{{ route('backend.products.create') }}">
                        <i class="fa fa-plus fa-fw"></i> Добавить товар
                    </a>
                </li>
                <li<?php if ($current_route == 'backend.categories.index' OR $current_route == 'backend.categories.descendants') echo ' class="active"' ?>>
                    <a href="{{ route('backend.categories.index') }}">
                        <i class="fa fa-fw fa-list-ul"></i> Управление категориями
                    </a>
                </li>
                <li<?php if ($current_route == 'backend.brands.index') echo ' class="active"' ?>>
                    <a href="{{ route('backend.brands.index') }}">
                        <i class="fa fa-fw fa-list-ul"></i> Управление брендами
                    </a>
                </li>
            </ul>
        </li>


        {{-- Публикации --}}

        <li<?php if ($module == 'publications') echo ' class="active"' ?>>
            <a href="#" class="dropdown-toggle collapsed" data-toggle="collapse" data-target="#publications">
                <i class="fa fa-fw fa-folder-open-o"></i> Публикации
            </a>
            <ul id="publications" class="collapse<?php if ($module == 'publications') echo ' in' ?>">
                <li<?php if ($current_route == 'backend.articles.index') echo ' class="active"' ?>>
                    <a href="{{ route('backend.articles.index') }}">
                        <i class="fa fa-fw fa-list-ul"></i> Управление статьями
                    </a>
                </li>
                <li<?php if ($current_route == 'backend.articles.create') echo ' class="active"' ?>>
                    <a href="{{ route('backend.articles.create') }}">
                        <i class="fa fa-plus fa-fw"></i> Добавить статью
                    </a>
                </li>
                <li<?php if ($current_route == 'backend.rubrics.index') echo ' class="active"' ?>>
                    <a href="{{ route('backend.rubrics.index') }}">
                        <i class="fa fa-fw fa-list-ul"></i> Управление рубриками
                    </a>
                </li>
            </ul>
        </li>


        {{-- Статические страницы --}}

        <li<?php if ($module == 'pages') echo ' class="active"' ?>>
            <a href="#" class="dropdown-toggle collapsed" data-toggle="collapse" data-target="#pages">
                <i class="fa fa-fw fa-files-o"></i> Статические страницы
            </a>
            <ul id="pages" class="collapse<?php if ($module == 'pages') echo ' in' ?>">
                <li<?php if ($current_route == 'backend.pages.index') echo ' class="active"' ?>>
                    <a href="{{ route('backend.pages.index') }}">
                        <i class="fa fa-fw fa-list-ul"></i> Управление страницами
                    </a>
                </li>
                <li<?php if ($current_route == 'backend.pages.create') echo ' class="active"' ?>>
                    <a href="{{ route('backend.pages.create') }}">
                        <i class="fa fa-plus fa-fw"></i> Добавить страницу
                    </a>
                </li>
            </ul>
        </li>

    </ul>
</div>
