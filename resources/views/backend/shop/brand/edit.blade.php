@extends('backend.layouts.default')

@section('title', 'Редактировать бренд')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header">
                Каталог товаров<small><i class="fa fa-angle-right fa-fw"></i>Редактировать бренд</small>
            </h1>
        </div>
    </div>

    {!! Form::model($brand, [
        'method' => 'PATCH',
        'route'  => ['backend.brands.update', $brand->id],
        'class'  => 'form-horizontal'
    ]) !!}

    @include ('backend.shop.brand._form')
    @include ('backend.form_buttons')

    {!! Form::close() !!}

@stop
