@extends('backend.layouts.default')

@section('title', 'Управление брендами')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header">
                Каталог товаров<small><i class="fa fa-angle-right fa-fw"></i>Управление брендами</small>
            </h1>
            <div class="btn-group">
                <a href="{{ route('backend.brands.create') }}" class="btn btn-success btn-sm">
                    <i class="fa fa-plus-circle fa-fw"></i> Добавить бренд
                </a>
            </div><hr>
        </div>
    </div>

    @if($brands->count())
        <table class="table table-striped table-hover">

            <thead>
            <tr>
                <th>Изображение</th>
                <th style="width: 75%">Бренд</th>
                <th>Действия</th>
            </tr>
            </thead>

            <tbody>
            @foreach($brands as $brand)
                <tr>
                    <td>
                        @if($brand->image)
                            <img src="{{ $brand->admin_image }}" alt="" width="52">
                        @endif
                    </td>

                    <td>
                        <a href="{{ route('backend.brands.edit', $brand->id) }}">
                            {{ $brand->title }}
                        </a><br>
                        <small>
                            Страна:
                            @if($brand->country->id == 1)
                                <span class="text-danger">{{ $brand->country->title }}</span>
                            @else
                                {{ $brand->country->title }}
                            @endif
                        </small>
                    </td>

                    <td class="icon_actions">
                        <a href="{{ route('backend.brands.edit', $brand->id ) }}" >
                            <i class="fa fa-pencil fa-lg fa-fw"></i></a>

                        {{--<a href="{{ route('brand', $brand->slug) }}" target="_blank">--}}
                            {{--<i class="fa fa-eye fa-lg fa-fw"></i></a>--}}

                        <a href="{{ route('backend.brands.confirm', $brand->id) }}">
                            <i class="fa fa-remove fa-lg fa-fw text-danger"></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>

        </table>
    @else
        <p>Список брендов пуст!</p>
    @endif

@stop
