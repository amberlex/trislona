@extends('backend.layouts.default')

@section('title', 'Удалить бренд' . $brand->title)

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header">
                Каталог товаров<small><i class="fa fa-angle-right fa-fw"></i>Удалить бренд</small>
            </h1>
        </div>
    </div>

    {!! Form::open([
        'method' => 'DELETE',
        'route'  => ['backend.brands.destroy', $brand->id]
    ]) !!}

    <div class="alert alert-danger">
        <p><strong>Внимание!</strong> Вы действительно хотите удалить этот бренд?</p>
        <p><i class="fa fa-file-o fa-fw"></i> {{ $brand->title }}</p>
    </div>

    {!! Form::submit('Да, удалить этот бренд!', ['class' => 'btn btn-danger']) !!}

    <a href="{{ route('backend.articles.index') }}" class="btn btn-success">
        <strong>Нет, вернуться назад!</strong>
    </a>

    {!! Form::close() !!}

@stop
