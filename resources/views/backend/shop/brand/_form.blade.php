<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active">
        <a href="#page" aria-controls="page" role="tab" data-toggle="tab">Бренд</a>
    </li>
    <li role="presentation">
        <a href="#seo" aria-controls="seo" role="tab" data-toggle="tab">SEO</a>
    </li>
</ul>
<br>

<div class="tab-content">
    <div role="tabpanel" class="tab-pane fade in active" id="page">

        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
            {!! Form::label('title', 'Название', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('title', null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('country_id', 'Страна', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::select('country_id', $countries, null, ['id' => 'country_id', 'class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('preview', 'Краткое описание', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::textarea('preview', null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('content', 'Полное описание', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('image', 'Изображение', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                @if (isset($brand))
                    @include('backend.form_image', ['object' => $brand])
                @else
                    <div class="alert alert-warning" role="alert">
                        Для загрузки изображения сохраните бренд.
                    </div>
                @endif
            </div>
        </div>

    </div>
    <div role="tabpanel" class="tab-pane fade" id="seo">

        @include('backend.form_seo')

    </div>
</div>

@section('scripts')
    <script src="/vendor/midium/laravel-ckeditor/ckeditor.js"></script>
    <script>
        $(function () {
            CKEDITOR.replace('preview', { customConfig: '/public/assets/backend/js/ck_preview.js' });
            CKEDITOR.replace('content', { customConfig: '/public/assets/backend/js/ck_content.js' });
            $('#country_id').select2();

            <?php if (isset($brand)): ?>
                var objectID = '{{ $brand->id }}';
                APP.imageUpload(objectID, '{{ route('backend.brands.img.upload', $brand->id) }}');
                $('#deleteBtn' + objectID).click(function(e) {
                    e.preventDefault();
                    APP.imageDelete(objectID, '{{ route('backend.brands.img.delete', $brand->id) }}');
                });
            <?php endif ?>
        });
    </script>
@stop
