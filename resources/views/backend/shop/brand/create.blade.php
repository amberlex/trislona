@extends('backend.layouts.default')

@section('title', 'Добавить бренд')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header">
                Каталог товаров<small><i class="fa fa-angle-right fa-fw"></i>Добавить бренд</small>
            </h1>
        </div>
    </div>

    {!! Form::open([
        'route'  => 'backend.brands.store',
        'class'  => 'form-horizontal'
    ]) !!}

    @include ('backend.shop.brand._form')
    @include ('backend.form_buttons')

    {!! Form::close() !!}

@stop
