@extends('backend.layouts.default')

@section('title', 'Управление товарами')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header">
                Каталог товаров<small><i class="fa fa-angle-right fa-fw"></i>Управление товарами</small>
            </h1>
            <div class="btn-group">
                <a href="{{ route('backend.products.create') }}" class="btn btn-success btn-sm">
                    <i class="fa fa-plus-circle fa-fw"></i> Добавить товар
                </a>
            </div><hr>
        </div>
    </div>

    @if($products->count())
        <table class="table table-striped table-hover">

            <thead>
            <tr>
                <th>Изображение</th>
                <th style="white-space: nowrap;">Название товара</th>
                <th>Категория</th>
                <th>Цена</th>
                <th>Код</th>
                <th>Статус</th>
                <th>Активный</th>
                <th>Действия</th>
            </tr>
            </thead>

            <tbody>
            @foreach($products as $product)
                <tr>
                    <td>
                        @if($product->image)
                            <img src="{{ $product->admin_image }}" alt="" width="52">
                        @endif
                    </td>
                    <td>
                        <a href="{{ route('backend.products.edit', $product->id) }}">
                            {{ $product->title }}
                        </a><br>
                        <small>
                            Бренд:
                            <a href="{{ route('backend.brands.edit', $product->brand->id) }}">
                                {{ $product->brand->title }}</a> <small>(Страна: {{ $product->brand->country->title }})</small>
                        </small>
                    </td>
                    <td>
                        <a href="{{ route('backend.categories.edit', $product->category->id) }}">
                            {{ $product->category->title }}</a>
                    </td>
                    <td>{{ $product->price }}</td>
                    <td>{{ $product->currency->code }}</td>
                    <td>
                        <button class="btn btn-xs btn-statuses btn-{{ $product->is_hit ? 'primary' : 'default' }}">
                            <i class="fa fa-fw fa-fire"></i>
                        </button>
                        <button class="btn btn-xs btn-statuses btn-{{ $product->is_hot ? 'primary' : 'default' }}">
                            <i class="fa fa-fw fa-gift"></i>
                        </button>
                        <button class="btn btn-xs btn-statuses btn-{{ $product->is_action ? 'primary' : 'default' }}">
                            <i class="fa fa-fw fa-star"></i>
                        </button>
                    </td>
                    <td>
                        @if($product->is_enabled)
                            <i class="fa fa-check-circle fa-lg fa-fw text-success"></i>
                        @else
                            <i class="fa fa-check-circle fa-lg fa-fw text-danger"></i>
                        @endif
                    </td>
                    <td class="icon_actions">
                        <a href="{{ route('backend.products.edit', $product->id ) }}" >
                            <i class="fa fa-pencil fa-lg fa-fw"></i></a>

                        <a href="{{ route('shop.product', $product->slug) }}" target="_blank">
                            <i class="fa fa-eye fa-lg fa-fw"></i></a>

                        <a href="{{ route('backend.products.confirm', $product->id) }}">
                            <i class="fa fa-remove fa-lg fa-fw text-danger"></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>

        </table>

        {!! $products->render() !!}

    @else
        <p>Товаров нет.</p>
    @endif

@stop
