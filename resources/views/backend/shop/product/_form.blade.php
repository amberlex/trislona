<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active">
        <a href="#page" aria-controls="page" role="tab" data-toggle="tab">Товар</a>
    </li>
    <li role="presentation">
        <a href="#seo" aria-controls="seo" role="tab" data-toggle="tab">SEO</a>
    </li>
</ul>
<br>

<div class="tab-content">
    <div role="tabpanel" class="tab-pane fade in active" id="page">

        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
            {!! Form::label('title', 'Название товара', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('title', null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
            {!! Form::label('category_id', 'Основная категория', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::select('category_id', [
                    '1' => 'Без категории'
                ] + $categoriesForSelect, null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('category_ids', 'Дополнительные', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::select('category_ids[]', $categoriesForSelect, null,
                ['id' => 'category_ids', 'class' => 'form-control', 'multiple']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('brand_id', 'Бренд', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::select('brand_id', $brandsForSelect, null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
            {!! Form::label('price', 'Цена', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('price', null, ['class' => 'form-control', 'placeholder' => '0.00']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('currency_id', 'Валюта', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::select('currency_id', $currenciesForSelect, null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('status', 'Статус', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                <label class="checkbox-inline">
                    {!! Form::hidden('is_hit', 0) !!}
                    {!! Form::checkbox('is_hit') !!} Хит
                </label>
                <label class="checkbox-inline">
                    {!! Form::hidden('is_hot', 0) !!}
                    {!! Form::checkbox('is_hot') !!} Новинка
                </label>
                <label class="checkbox-inline">
                    {!! Form::hidden('is_action', 0) !!}
                    {!! Form::checkbox('is_action') !!} Акция
                </label>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('published_at', 'На сайте', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                <div class="checkbox">
                    <label>
                        {!! Form::hidden('is_enabled', 0) !!}
                        {!! Form::checkbox('is_enabled') !!} Активный
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('preview', 'Краткое описание', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::textarea('preview', null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('content', 'Полное описание', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('image', 'Изображение', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                @if (isset($product))
                    @include('backend.form_image', ['object' => $product])
                @else
                    <div class="alert alert-warning" role="alert">
                        Для загрузки изображения сохраните товар.
                    </div>
                @endif
            </div>
        </div>

    </div>
    <div role="tabpanel" class="tab-pane fade" id="seo">

        @include('backend.form_seo')

    </div>
</div>

@section('scripts')
    <script src="/vendor/midium/laravel-ckeditor/ckeditor.js"></script>
    <script>
        $(function () {
            CKEDITOR.replace('preview', { customConfig: '/public/assets/backend/js/ck_preview.js' });
            CKEDITOR.replace('content', { customConfig: '/public/assets/backend/js/ck_content.js' });
            $('#brand_id').select2();
            $('#category_id').select2();
            $('#category_ids').select2({
                tags: true,
                placeholder: "Выберете дополнительные категории"
            });

            <?php if (isset($product)): ?>
                var objectID = '{{ $product->id }}';
                APP.imageUpload(objectID, '{{ route('backend.products.img.upload', $product->id) }}');
                $('#deleteBtn' + objectID).click(function(e) {
                    e.preventDefault();
                    APP.imageDelete(objectID, '{{ route('backend.products.img.delete', $product->id) }}');
                });
            <?php endif ?>
        });
    </script>
@stop
