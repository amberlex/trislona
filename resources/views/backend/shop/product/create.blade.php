@extends('backend.layouts.default')

@section('title', 'Добавить товар')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header">
                Каталог товаров<small><i class="fa fa-angle-right fa-fw"></i>Добавить товар</small>
            </h1>
        </div>
    </div>

    {!! Form::open([
        'route'  => 'backend.products.store',
        'class'  => 'form-horizontal'
    ]) !!}

    @include ('backend.shop.product._form')
    @include ('backend.form_buttons')

    {!! Form::close() !!}

@stop
