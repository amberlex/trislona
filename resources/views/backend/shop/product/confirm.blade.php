@extends('backend.layouts.default')

@section('title', 'Удалить товар' . $product->title)

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header">
                Каталог товаров<small><i class="fa fa-angle-right fa-fw"></i>Удалить товар</small>
            </h1>
        </div>
    </div>

    {!! Form::open([
        'method' => 'DELETE',
        'route'  => ['backend.products.destroy', $product->id]
    ]) !!}

    <div class="alert alert-danger">
        <p><strong>Внимание!</strong> Вы действительно хотите удалить этот товар?</p>
        <p><i class="fa fa-file-o fa-fw"></i> {{ $product->title }}</p>
    </div>

    {!! Form::submit('Да, удалить этот товар!', ['class' => 'btn btn-danger']) !!}

    <a href="{{ route('backend.products.index') }}" class="btn btn-success">
        <strong>Нет, вернуться назад!</strong>
    </a>

    {!! Form::close() !!}

@stop
