@extends('backend.layouts.default')

@section('title', 'Редактировать товар')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header">
                Каталог товаров<small><i class="fa fa-angle-right fa-fw"></i>Редактировать товар</small>
            </h1>
        </div>
    </div>

    {!! Form::model($product, [
        'method' => 'PATCH',
        'route'  => ['backend.products.update', $product->id],
        'class'  => 'form-horizontal'
    ]) !!}

    @include ('backend.shop.product._form')
    @include ('backend.form_buttons')

    {!! Form::close() !!}

@stop
