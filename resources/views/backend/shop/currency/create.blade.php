@extends('backend.layouts.default')

@section('title', 'Добавить валюту')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header">
                Настройки<small><i class="fa fa-angle-right fa-fw"></i>Добавить валюту</small>
            </h1>
        </div>
    </div>

    {!! Form::open([
        'route'  => 'backend.currencies.store',
        'class'  => 'form-horizontal'
    ]) !!}

    @include ('backend.shop.currency._form')
    @include ('backend.form_buttons')

    {!! Form::close() !!}

@stop
