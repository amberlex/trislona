@extends('backend.layouts.default')

@section('title', 'Редактировать валюту')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header">
                Настройки<small><i class="fa fa-angle-right fa-fw"></i>Редактировать валюту</small>
            </h1>
        </div>
    </div>

    {!! Form::model($currency, [
        'method' => 'PATCH',
        'route'  => ['backend.currencies.update', $currency->id],
        'class'  => 'form-horizontal'
    ]) !!}

    @include ('backend.shop.currency._form')
    @include ('backend.form_buttons')

    {!! Form::close() !!}

@stop
