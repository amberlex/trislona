@extends('backend.layouts.default')

@section('title', 'Управление валютами')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header">
                Настройки<small><i class="fa fa-angle-right fa-fw"></i>Управление валютами</small>
            </h1>
            <div class="btn-group">
                <a href="{{ route('backend.currencies.create') }}" class="btn btn-success btn-sm">
                    <i class="fa fa-plus-circle fa-fw"></i> Добавить валюту
                </a>
            </div><hr>
        </div>
    </div>

    <table class="table table-striped table-hover">

        <thead>
        <tr>
            <th>Название</th>
            <th>ISO Код</th>
            <th>Символ</th>
            <th>Курс</th>
            <th>Обновлен</th>
            <th>Основная</th>
            <th>Используется</th>
            <th>Действия</th>
        </tr>
        </thead>

        <tbody>
        @foreach($currencies as $currency)
            <tr>
                <td>
                    <a href="{{ route('backend.currencies.edit', $currency->id ) }}">
                        {{ $currency->name }}
                    </a>
                </td>
                <td>{{ $currency->code }}</td>
                <td>{{ $currency->symbol }}</td>
                <td>{{ $currency->rate }}</td>
                <td>{{ $currency->getForDisplay() }}</td>
                <td>
                    @if($currency->is_main)
                        <i class="fa fa-check-circle fa-lg fa-fw text-success"></i>
                    @else
                        <i class="fa fa-check-circle fa-lg fa-fw text-danger"></i>
                    @endif
                </td>
                <td>
                    @if($currency->is_enabled)
                        <i class="fa fa-check-circle fa-lg fa-fw text-success"></i>
                    @else
                        <i class="fa fa-check-circle fa-lg fa-fw text-danger"></i>
                    @endif
                </td>
                <td class="icon_actions">

                    <a href="{{ route('backend.currencies.edit', $currency->id ) }}" >
                        <i class="fa fa-pencil fa-lg fa-fw"></i></a>

                    @unless($currency->is_main)
                        <a href="{{ route('backend.currencies.confirm', $currency->id) }}">
                            <i class="fa fa-remove fa-lg fa-fw text-danger"></i></a>
                    @endunless

                </td>
            </tr>
        @endforeach
        </tbody>

    </table>

@stop
