<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    {!! Form::label('name', 'Название', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
    {!! Form::label('code', 'ISO Код', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::text('code', null, ['class' => 'form-control']) !!}
    </div>
</div>

@if(isset($currency) AND $currency->is_main)
    <div class="form-group has-success{{ $errors->has('rate') ? ' has-error' : '' }}">
        {!! Form::label('rate', 'Основная валюта', ['class' => 'col-md-2 control-label text-success']) !!}
        <div class="col-md-10">
            {!! Form::text('rate', null, ['class' => 'form-control']) !!}
        </div>
    </div>
@else
    <div class="form-group{{ $errors->has('rate') ? ' has-error' : '' }}">
        {!! Form::label('rate', 'Курс к основной валюте', ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-10">
            {!! Form::text('rate', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
            <!-- Minfin.com.ua calc informer 200x112 yellow-->
            <div id="minfin-informer-m1Fn-calc">Загружаем
                <a href="http://minfin.com.ua/currency/" target="_blank">курсы валют</a> <a>от minfin.com.ua</a>
            </div>
            <script type="text/javascript">var iframe = '<ifra'+'me width="200" height="112" fram'+'eborder="0" src="http://informer.minfin.com.ua/gen/calc/nbu/?color=yellow" vspace="0" scrolling="no" hspace="0" allowtransparency="true"style="width:200px;height:112px;ove'+'rflow:hidden;"></iframe>';var cl = 'minfin-informer-m1Fn-calc';document.getElementById(cl).innerHTML = iframe; </script>
            <noscript><img src="http://informer.minfin.com.ua/gen/img.png" width="1" height="1" alt="minfin.com.ua: курсы валют" title="Курс валют" border="0" /></noscript>
            <!-- Minfin.com.ua calc informer 200x112 yellow-->
        </div>
    </div>
@endif

<div class="form-group{{ $errors->has('symbol') ? ' has-error' : '' }}">
    {!! Form::label('symbol', 'Символ', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::text('symbol', null, ['class' => 'form-control']) !!}
        <span class="help-block">
            Перечень возможных кодов валют приведен в международном стандарте
            <a href="http://www.currency-iso.org/dam/downloads/lists/list_one.xml">ISO 4217</a>
        </span>
    </div>
</div>

<div class="form-group">
    {!! Form::label('symbol_pos', 'Позиция символа', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        {!! Form::select('symbol_pos', ['left' => 'Слева', 'right' => 'Справа'], null, ['class' => 'form-control']) !!}
    </div>
</div>

@unless(isset($currency) AND $currency->is_main)
    <div class="form-group">
        <div class="col-md-10 col-md-offset-2">
            <div class="checkbox">
                <label>
                    {!! Form::hidden('is_main', 0) !!}
                    {!! Form::checkbox('is_main') !!} Сделать основной валютой
                </label>
            </div>
        </div>
    </div>
@endunless

<div class="form-group">
    <div class="col-md-10 col-md-offset-2">
        <div class="checkbox">
            <label>
                {!! Form::hidden('is_enabled', 0) !!}
                {!! Form::checkbox('is_enabled') !!} Используется
            </label>
        </div>
    </div>
</div>
