@extends('backend.layouts.default')

@section('title', 'Удалить ' . $currency->title)

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header">
                Настройки<small><i class="fa fa-angle-right fa-fw"></i>Удалить валюту</small>
            </h1>
        </div>
    </div>

    @if($currency->is_main)

        <div class="alert alert-warning">
            <p>Вы не можете удалить основную валюту.</p>
        </div>
        <a href="{{ route('backend.currencies.index') }}" class="btn btn-success">
            <strong>Вернуться назад</strong>
        </a>

    @else

        {!! Form::open([
           'method' => 'DELETE',
           'route'  => ['backend.currencies.destroy', $currency->id]
       ]) !!}

        <div class="alert alert-danger">
            <p><strong>Внимание!</strong> Вы действительно хотите удалить эту валюту?</p>
            <p>
                <i class="fa fa-angle-right fa-fw"></i> ISO Код:  <strong>{{ $currency->code }}  </strong><br>
                <i class="fa fa-angle-right fa-fw"></i> Название: <strong>{{ $currency->name }}  </strong><br>
                <i class="fa fa-angle-right fa-fw"></i> Символ:   <strong>{{ $currency->symbol }}</strong>
            </p>
        </div>

        {!! Form::submit('Да, удалить эту валюту!', ['class' => 'btn btn-danger']) !!}

        <a href="{{ route('backend.currencies.index') }}" class="btn btn-success">
            <strong>Нет, вернуться назад!</strong>
        </a>

        {!! Form::close() !!}

    @endif

@stop
