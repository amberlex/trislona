@extends('backend.layouts.default')

@section('title', 'Удалить категорию' . $category->title)

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header">
                Каталог товаров<small><i class="fa fa-angle-right fa-fw"></i>Удалить рубрику</small>
            </h1>
        </div>
    </div>

    {!! Form::open([
        'method' => 'DELETE',
        'route'  => ['backend.categories.destroy', $category->id]
    ]) !!}

    <div class="alert alert-danger">
        <p><strong>Внимание!</strong> Вы действительно хотите удалить эту категорию?</p>
        <p><i class="fa fa-folder-open-o fa-fw"></i> {{ $category->title }}</p>
    </div>

    {!! Form::submit('Да, удалить эту категорию!', ['class' => 'btn btn-danger']) !!}

    <a href="{{ route('backend.categories.index') }}" class="btn btn-success">
        <strong>Нет, вернуться назад!</strong>
    </a>

    {!! Form::close() !!}

@stop
