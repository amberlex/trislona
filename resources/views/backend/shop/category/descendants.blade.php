@extends('backend.layouts.default')

@section('title', 'Управление категориями')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header">
                Каталог товаров<small><i class="fa fa-angle-right fa-fw"></i>Управление категориями</small>
            </h1>
            <div class="btn-group">
                <a href="{{ route('backend.categories.create') }}" class="btn btn-success btn-sm">
                    <i class="fa fa-plus-circle fa-fw"></i> Добавить категорию
                </a>
            </div><hr>
        </div>
    </div>

    <a href="{{ route('backend.categories.index') }}" class="btn btn-sm btn-categories btn-default">
        <i class="fa fa-fw fa-list"></i> Главные категории
    </a>
    <?php foreach($categories as $category) {
        echo link_to_route('backend.categories.descendants', $category->title,
            ['id' => $category->id],
            ['class' => 'btn btn-sm btn-categories btn-' . ($category->id == $category_id ? 'primary' : 'default')]
        );
    } ?>
    <hr>

    @if($descendants->count())
        <table class="table table-striped table-hover">

            <thead>
            <tr>
                <th>Изображение</th>
                <th style="width: 90%; white-space: nowrap;">Подкатегории</th>
                <th>Действия</th>
            </tr>
            </thead>

            <tbody>
            @foreach($descendants as $descendant)
                <tr>
                    <td>
                        @if($descendant->image)
                            <img src="{{ $descendant->admin_image }}" alt="" width="32">
                        @endif
                    </td>
                    <td>
                        {!! $descendant->linkToPaddedTitle(
                            route('backend.categories.edit', $descendant->id )
                        ) !!}
                    </td>
                    <td class="icon_actions">
                        <a href="{{ route('backend.categories.edit', $descendant->id ) }}" >
                            <i class="fa fa-pencil fa-lg fa-fw"></i></a>

                        <a href="{{ route('shop.category', $descendant->slug) }}" target="_blank">
                            <i class="fa fa-eye fa-lg fa-fw"></i></a>

                        <a href="{{ route('backend.categories.confirm', $descendant->id) }}">
                            <i class="fa fa-remove fa-lg fa-fw text-danger"></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>

        </table>
    @else
        <p>Категорий нет.</p>
    @endif

@stop
