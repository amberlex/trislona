<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active">
        <a href="#page" aria-controls="page" role="tab" data-toggle="tab">Категория</a>
    </li>
    <li role="presentation">
        <a href="#seo" aria-controls="seo" role="tab" data-toggle="tab">SEO</a>
    </li>
</ul>
<br>

<div class="tab-content">
    <div role="tabpanel" class="tab-pane fade in active" id="page">
        {!! Form::hidden('path', null) !!}
        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
            {!! Form::label('title', 'Название', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('title', null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('description', 'Описание', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('order_category', 'Расположение', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-4">
                        {!! Form::select('order', [
                            ''        => '',
                            'before'  => 'Перед категорией',
                            'after'   => 'После категории',
                            'childOf' => 'В категорию'
                        ], null, ['class' => 'form-control']) !!}
                    </div>
                    <div class="col-md-8">
                        {!! Form::select('order_category', [
                            '' => ''
                        ] + $categoriesForSelect, null, ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('image', 'Изображение', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                @if (isset($category))
                    @include('backend.form_image', ['object' => $category])
                @else
                    <div class="alert alert-warning" role="alert">
                        Для загрузки изображения сохраните категорию.
                    </div>
                @endif
            </div>
        </div>

    </div>
    <div role="tabpanel" class="tab-pane fade" id="seo">

        @include('backend.form_seo')

    </div>
</div>

@section('scripts')
    <script src="/vendor/midium/laravel-ckeditor/ckeditor.js"></script>
    <script>
        $(function () {
            CKEDITOR.replace('description', { customConfig: '/public/assets/backend/js/ck_preview.js' });
            $('#order_category').select2();

            <?php if (isset($category)): ?>
                var objectID = '{{ $category->id }}';
                APP.imageUpload(objectID, '{{ route('backend.categories.img.upload', $category->id) }}');
                $('#deleteBtn' + objectID).click(function(e) {
                    e.preventDefault();
                    APP.imageDelete(objectID, '{{ route('backend.categories.img.delete', $category->id) }}');
                });
            <?php endif ?>
        });
    </script>
@stop
