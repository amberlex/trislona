@extends('backend.layouts.default')

@section('title', 'Добавить категорию')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header">
                Каталог товаров
                <small>
                    <i class="fa fa-angle-right fa-fw"></i>
                    Добавить категорию
                </small>
            </h1>
        </div>
    </div>

    {!! Form::open([
        'route'  => 'backend.categories.store',
        'class'  => 'form-horizontal'
    ]) !!}

    @include ('backend.shop.category._form')
    @include ('backend.form_buttons')

    {!! Form::close() !!}

@stop
