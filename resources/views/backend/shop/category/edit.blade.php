@extends('backend.layouts.default')

@section('title', 'Редактировать категорию')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header">
                Каталог товаров
                <small>
                    <i class="fa fa-angle-right fa-fw"></i>
                    Редактировать категорию
                </small>
            </h1>
        </div>
    </div>

    {!! Form::model($category, [
        'method' => 'PATCH',
        'route'  => ['backend.categories.update', $category->id],
        'class'  => 'form-horizontal'
    ]) !!}

    @include ('backend.shop.category._form')
    @include ('backend.form_buttons')

    {!! Form::close() !!}

@stop
