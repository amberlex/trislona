<div class="form-group">
    <div class="col-md-offset-2 col-md-10">
        <hr>
        <a href="{{ session('backUrl') }}"><i class="fa fa-long-arrow-left fa-fw"></i>Вернуться</a>&nbsp;&nbsp;&nbsp;
        <button type="submit" class="btn btn-primary">
            <i class="fa fa-save fa-fw"></i>
            Сохранить
        </button>
        <button type="submit" name="save_and_exit" value="ok" class="btn btn-default">
            <i class="fa fa-save fa-fw"></i>
            Сохранить и выйти
        </button>
    </div>
</div>
