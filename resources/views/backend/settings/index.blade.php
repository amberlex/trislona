@extends('backend.layouts.default')

@section('title', 'Настройки')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header">
                Настройки<small><i class="fa fa-angle-right fa-fw"></i>Настройки сайта</small>
            </h1>
        </div>
    </div>

    <table class="table table-striped table-hover">

        <thead>
        <tr>
            <th>Тип</th>
            <th>Ключ</th>
            <th>Название</th>
            <th>Значение</th>
            <th>Статус</th>
            <th>Действия</th>
        </tr>
        </thead>

        <tbody>
        @foreach($settings as $setting)
            <tr>
                <td>{{ $setting->type }}</td>
                <td>{{ $setting->key }}</td>
                <td>
                    <a href="{{ route('backend.settings.edit', $setting->id ) }}">
                        {{ $setting->name }}
                    </a>
                </td>
                <td>{{ $setting->val }}</td>
                <td>
                    @if($setting->is_enabled)
                        <i class="fa fa-check-circle fa-lg fa-fw text-success"></i>
                    @else
                        <i class="fa fa-check-circle fa-lg fa-fw text-danger"></i>
                    @endif
                </td>
                <td>
                    <a href="{{ route('backend.settings.edit', $setting->id ) }}" >
                        <i class="fa fa-pencil fa-lg fa-fw"></i></a>
                </td>
            </tr>
        @endforeach
        </tbody>

    </table>

@stop
