<div class="form-group">
    {!! Form::label('val', $setting->name, ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-10">
        @if($setting->field == 'input')
            {!! Form::text('val', null, ['class' => 'form-control']) !!}
        @elseif($setting->field == 'textarea')
            {!! Form::textarea('val', null, ['class' => 'form-control', 'rows' => 4]) !!}
        @endif
    </div>
</div>

<div class="form-group">
    <div class="col-md-10 col-md-offset-2">
        <div class="checkbox">
            <label>
                {!! Form::hidden('is_enabled', 0) !!}
                {!! Form::checkbox('is_enabled') !!} Статус
            </label>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-2 col-md-10">
        <hr>
        {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary']) !!}
    </div>
</div>