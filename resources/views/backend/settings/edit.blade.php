@extends('backend.layouts.default')

@section('title', 'Изменить параметр')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-header">
                Настройки<small><i class="fa fa-angle-right fa-fw"></i>Изменить параметр</small>
            </h1>
            <div class="btn-group">
                <a href="{{ route('backend.settings.index') }}" class="btn btn-default btn-sm">
                    <i class="fa fa-angle-left fa-fw"></i> Назад
                </a>
            </div><hr>
        </div>
    </div>

    {!! Form::model($setting, [
        'method' => 'PATCH',
        'route'  => ['backend.settings.update', $setting->id],
        'class'  => 'form-horizontal'
    ]) !!}

    @include ('backend.settings._form', ['submitButtonText' => 'Обновить'])

    {!! Form::close() !!}

@stop
