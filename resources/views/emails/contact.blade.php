<p>
    Вы получили новое сообщение с вашего веб-сайта через контактную форму.
</p>
<p>
    Подробности:
</p>
<ul>
    <li>Имя: <strong>{{ $name }}</strong></li>
    <li>Email: <strong>{{ $email }}</strong></li>
</ul>

<hr>
<p>
    @foreach ($messageLines as $messageLine)
        {{ $messageLine }}<br>
    @endforeach
</p>
<hr>
