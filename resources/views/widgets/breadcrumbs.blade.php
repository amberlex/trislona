<ol class="breadcrumb">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    @foreach($breadcrumbs as $breadcrumb)
        @if(isset($breadcrumb['url']))
            <li><a href="{{ $breadcrumb['url'] }}">{{ $breadcrumb['label'] }}</a></li>
        @else
            <li class="active">{{ $breadcrumb['label'] }}</li>
        @endif
    @endforeach
</ol>
