<ul class="nav nav-tabs">
    <li class="active">
        <a href="#popular_products" data-toggle="tab">Хиты</a>
    </li>
    <li>
        <a href="#new_products" data-toggle="tab">Новинки</a>
    </li>
    <li>
        <a href="#action_products" data-toggle="tab">Спецпредложения</a>
    </li>
</ul>
<br>

<div class="tab-content">
    <div class="tab-pane fade in active" id="popular_products">
        <div class="row">
            @foreach($hits as $hit)
                <div class="col-sm-3 col-md-3 col-lg-2">
                    <div class="thumbnail">
                        <img src="{{ $hit->th_image }}" alt="{{ $hit->title }}">
                        <div class="caption">
                            <h3><a href="{{ route('shop.product', $hit->slug) }}">{{ $hit->title }}</a></h3>
                            <p><strong>Цена:</strong> <span class="label label-primary">{!! $hit->priceOnPageUAN !!}</span></p>
                            <a href="#" class="btn-add-to-cart btn btn-success btn-xs">
                                <i class="fa fa-plus fa-fw"></i> В корзину
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="tab-pane fade" id="new_products">
        <div class="row">
            @foreach($hots as $hot)
                <div class="col-sm-3 col-md-3 col-lg-2">
                    <div class="thumbnail">
                        <img src="{{ $hot->th_image }}" alt="{{ $hot->title }}">
                        <div class="caption">
                            <h3><a href="{{ route('shop.product', $hot->slug) }}">{{ $hot->title }}</a></h3>
                            <p><strong>Цена:</strong> <span class="label label-primary">{!! $hot->priceOnPageUAN !!}</span></p>
                            <a href="#" class="btn-add-to-cart btn btn-success btn-xs">
                                <i class="fa fa-plus fa-fw"></i> В корзину
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="tab-pane fade" id="action_products">
        <div class="row">
            @foreach($actions as $action)
                <div class="col-sm-3 col-md-3 col-lg-2">
                    <div class="thumbnail">
                        <img src="{{ $action->th_image }}" alt="{{ $action->title }}">
                        <div class="caption">
                            <h3><a href="{{ route('shop.product', $action->slug) }}">{{ $action->title }}</a></h3>
                            <p><strong>Цена:</strong> <span class="label label-primary">{!! $action->priceOnPageUAN !!}</span></p>
                            <a href="#" class="btn-add-to-cart btn btn-success btn-xs">
                                <i class="fa fa-plus fa-fw"></i> В корзину
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
