<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">{{ $title  }}</h3>
    </div>
    <div class="panel-body">
        @foreach($categories as $category)
            @if($category['depth'] == 2)
                <i class="fa fa-fw fa-angle-right"></i>
            @endif
            <a href="{{ route('shop.category', ['slug' => $category['slug']]) }}">
                {{ $category['title'] }}</a>
            <br>
        @endforeach
    </div>
</div>
