<div class="list-group">
    <a href="{{ route('shop.categories') }}" class="list-group-item">
        <i class="fa fa-folder-o fa-fw"></i> Категории товаров
    </a>
    <a href="{{ route('shop.brands') }}" class="list-group-item">
        <i class="fa fa-apple fa-fw"></i> Бренды
    </a>
    @foreach($rubrics as $rubric)
        <a href="{{ route('publications.rubric', $rubric->slug) }}" class="list-group-item">
            <i class="fa {{ $rubric->icon }} fa-fw"></i> {{ $rubric->title }}
        </a>
    @endforeach
    @foreach($pages as $page)
        <a href="{{ route('page', ['slug' => $page->slug]) }}" class="list-group-item">
            <i class="fa {{ $page->icon }} fa-fw"></i> {{ $page->title }}
        </a>
    @endforeach
    <a href="{{ route('contact.create') }}" class="list-group-item">
        <i class="fa fa-envelope-o fa-fw"></i> Контакты
    </a>
</div>
