<h2 class="page-header">
    Последние публикации
    <small>
        <i class="fa fa-angle-right fa-fw"></i>
        <a href="{{ route('publications.articles') }}">Все публикации</a>
    </small>
</h2>

@foreach($articles as $article)
    <div class="row">
        <div class="col-sm-3">
            @if($article->image)
                <a href="{{ route('publications.article', $article->slug)  }}" class="thumbnail">
                    <img src="{{ $article->th_image }}" alt="{{ $article->title }}">
                </a>
            @endif
        </div>
        <div class="col-sm-9">
            <h3>
                {!! Html::linkRoute('publications.article', $article->title, $article->slug) !!}
            </h3>
            {!! $article->preview !!}
            <p>
                <i class="fa fa-folder-o fa-fw"></i>
                {!! Html::linkRoute('publications.rubric', $article->rubric->title, $article->rubric->slug) !!}
                <i class="fa fa-calendar fa-fw"></i>
                <small>{{ $article->published_at }}</small>
            </p>
        </div>
    </div>
@endforeach
