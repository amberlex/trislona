<h2 class="page-header">
    Бренды
    <small>
        <i class="fa fa-angle-right fa-fw"></i>
        <a href="{{ route('shop.brands') }}">Все бренды</a>
    </small>
</h2>

<div class="row">
    @foreach($brands as $brand)

        <div class="col-xs-3 col-sm-2 col-md-2 col-lg-1">
            <a href="{{ route('shop.brand', ['slug' => $brand->slug]) }}" class="thumbnail">
                <img src="{{ $brand->th_image }}" alt="{{ $brand->title }}">
            </a>
        </div>

    @endforeach
</div>
