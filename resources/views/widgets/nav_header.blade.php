@if(count($pages))
    @foreach($pages as $page)
        <div class="nowrap">
            <i class="fa {{ $page->icon }} fa-fw"></i>
            <a href="{{ route('page', ['slug' => $page->slug]) }}">{{ $page->title }}</a>
        </div>
    @endforeach
    <br>
@endif

@if(count($pages))
    @foreach($rubrics as $rubric)
        <div class="nowrap">
            <i class="fa {{ $rubric->icon }} fa-fw"></i>
            <a href="{{ route('publications.rubric', $rubric->slug) }}">{{ $rubric->title }}</a>
        </div>
    @endforeach
    <br>
@endif

<div class="nowrap">
    <i class="fa fa-folder-o fa-fw"></i>
    <a href="{{ route('shop.categories') }}">Категории</a>
</div>

<div class="nowrap">
    <i class="fa fa-apple fa-fw"></i>
    <a href="{{ route('shop.brands') }}">Бренды</a>
</div>

<div class="nowrap">
    <i class="fa fa-envelope-o fa-fw"></i>
    <a href="{{ route('contact.create') }}">Контакты</a>
</div>