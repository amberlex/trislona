var gulp = require('gulp');
var elixir = require('laravel-elixir');

// Command: gulp copyfiles
//
gulp.task("copyfiles", function() {

    // Fonts
    gulp.src("vendor/bower_components/bootstrap/dist/fonts/**")
        .pipe(gulp.dest("public/assets/fonts"))
        .pipe(gulp.dest("public/assets/backend/fonts"));

    gulp.src("vendor/bower_components/font-awesome/fonts/**")
        .pipe(gulp.dest("public/assets/fonts"))
        .pipe(gulp.dest("public/assets/backend/fonts"));


    // JS
    gulp.src("vendor/bower_components/jquery/dist/jquery.js")
        .pipe(gulp.dest("resources/assets/js/"))
        .pipe(gulp.dest("resources/assets/backend/js/"));

    gulp.src("vendor/bower_components/moment/min/moment-with-locales.min.js")
        .pipe(gulp.dest("resources/assets/backend/js/"));

    gulp.src("vendor/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js")
        .pipe(gulp.dest("resources/assets/backend/js/"));

    gulp.src("vendor/bower_components/bootstrap/dist/js/bootstrap.js")
        .pipe(gulp.dest("resources/assets/backend/js/"));


    // CSS
    gulp.src("vendor/bower_components/bootstrap/dist/css/bootstrap.css")
        .pipe(gulp.dest("resources/assets/css/"))
        .pipe(gulp.dest("resources/assets/backend/css/"));

    gulp.src("vendor/bower_components/font-awesome/css/font-awesome.css")
        .pipe(gulp.dest("resources/assets/css/"))
        .pipe(gulp.dest("resources/assets/backend/css/"));

    gulp.src("vendor/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css")
        .pipe(gulp.dest("resources/assets/backend/css/"));

});

// disable map
elixir.config.sourcemaps = false;

elixir(function(mix) {

    //------------------------------------------------------------------------------------------------------------------
    // Frontend

    // Combine styles
    mix.styles([
            '/bootstrap.css',
            '/ie10-viewport-bug-workaround.css',
            '/font-awesome.css',
            '/app.css'
        ],
        'public/assets/css/all.css', // result file
        'resources/assets/css'       // from
    );

    // Combine scripts
    mix.scripts([
            'jquery.js',
            'bootstrap.js',
            'ie10-viewport-bug-workaround.js',
            'app.js'
        ],
        'public/assets/js/all.js', // result file
        'resources/assets/js'      // from
    );


    //------------------------------------------------------------------------------------------------------------------
    // Backend

    // Compiled LESS
    mix.less([
            'admin-variables.less',
            'admin.less'
        ],
        'resources/assets/backend/css/admin.css' // result file
    );

    // Combine styles
    mix.styles([
            'bootstrap.css',
            'ie10-viewport-bug-workaround.css',
            'font-awesome.css',
            'bootstrap-datetimepicker.min.css',
            'select2.css',
            'admin.css'
        ],
        'public/assets/backend/css/all.css', // result file
        'resources/assets/backend/css'       // from
    );

    // Combine scripts
    mix.scripts([
            'jquery.js',
            'bootstrap.js',
            'ie10-viewport-bug-workaround.js',
            'moment-with-locales.min.js',
            'bootstrap-datetimepicker.min.js',
            'select2.js',
            'SimpleAjaxUploader.js',
            'admin.js'
        ],
        'public/assets/backend/js/all.js', // result file
        'resources/assets/backend/js'      // from
    );

});